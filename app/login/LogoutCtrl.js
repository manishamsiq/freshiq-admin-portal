/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.login')
.controller('LogoutCtrl', function(UserService, config, $state) {
  // adding a logout event to notify other tabs.
  localStorage.setItem('logout-event', 'logout' + Math.random());

  UserService.logout().finally(function() {
    if (config.logout_redirect_url) {
      return location.href = config.logout_redirect_url;
    } else {
      return $state.go('login');
    }
  });

  return this;
});
