/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.signup')
.controller('ConnectToZendeskCtrl',
function(SignupService, FlashService, $state, $stateParams) {

  this.subdomain = $stateParams.subdomain;

  const oauth = () => {
    this.loading = true;
    const redirectTo = $state.href('signup.new-user');
    const failureTo = $state.href('signup.connect-to-zendesk',
      {errorMessage: "Error connecting to Zendesk"});

    return location.href = SignupService.zendeskOauthURL(this.subdomain,
                                                  redirectTo,
                                                  failureTo);
  };

  const api = () => {
    this.loading = true;

    return SignupService.connectWithAPIKey(this.subdomain, this.email, this.apiToken)
      .success(() => $state.go('signup.new-user')).error(response => {
        this.loading = false;
        const message = (response.data != null ? response.data.message : undefined) || "Connecting to Zendesk failed.";
        return FlashService.flash(message, "error");
    });
  };

  this.formSubmit = function() {
    if (this.api) {
      return api();
    } else {
      return oauth();
    }
  };

  return this;
});
