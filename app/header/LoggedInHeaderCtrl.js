/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport')
.controller('LoggedInHeaderCtrl', function($stateParams, ClientService, UserService) {
  this.clientId = parseInt($stateParams.clientId);

  if (this.clientId) {
    UserService.me().then(me => {
      if (me.client_id !== this.clientId) {
        return ClientService.get(this.clientId).then(client => {
          return this.client = client;
        });
      }
    });
  }

  return this;
});
