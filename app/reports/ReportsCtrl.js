/*
 * decaffeinate suggestions:
 * DS101: Remove unnecessary use of Array.from
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.reports')
.controller('ReportsCtrl', function(
  client, route, MacroModelService,
  TicketFieldService, $sce, $q, $timeout) {

  this.client = client;
  this.initial_loading = true;
  this.show_response = null;
  this.show_triage = null;
  this.show_ar = null;
  this.report_type = null;
  this.report_url = null;

  this.setup = () => {
    return $q.all([
      // fetch enabled ticket fields
      TicketFieldService.enabledTicketFields(this.client.id),
      // fetch response enabled
      MacroModelService.macroModels(this.client)
    ]).then(values => {
      let [ticket_fields, macro_models] = Array.from(values);
      ticket_fields = _.filter(ticket_fields, tf => tf.enabled);
      macro_models = _.filter(macro_models, mm => mm.enabled);
      const ar_models = _.filter(macro_models, mm => mm.auto_response_enabled);
      this.show_triage = ticket_fields.length > 0;
      this.show_response = macro_models.length > 0;
      this.show_ar = true || (ar_models.length > 0);

      // select active tab
      if (this.show_response) {
        this.report_type = 'response';
      } else if (this.show_triage) {
        this.report_type = 'triage';
      } else if (this.show_ar) {
        this.report_type = 'ar';
      }

      this.initial_loading = false;

      return this.hideSpinner();
    });
  };

  this.showSpinner = function() {
    const elem = document.querySelector('.loading');
    if (elem) {
      return elem.style.display = 'inline';
    }
  };

  this.hideSpinner = function() {
    const delay = (ms, f) => $timeout(f, ms);
    return delay(50, function() {
      const elem = document.querySelector('.loading');
      if (elem) {
        return elem.style.display = 'none';
      }
    });
  };

  this.setup();

  return this;
});
