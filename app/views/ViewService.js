/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.views')
.service('ViewService', ($http, $q, config, TicketFieldService) => ({
  viewTemplates(clientId){
    return TicketFieldService.wiseConfidentField(clientId).then(function(wise_confident_field) {

      // Wise Confident Response
      let wise_confident_foreign_id, wise_confident_note;
      if (wise_confident_field != null ? wise_confident_field.foreign_id : undefined) {
        wise_confident_foreign_id = wise_confident_field.foreign_id;
        wise_confident_note = `NOTE: The \"Wise Confident Response\" field was found with foreign_id=${wise_confident_foreign_id}.\nPlease double check and change it if needed.`;
      } else {
        wise_confident_foreign_id = "REPLACE_WITH_MACRO_FOREIGN_ID";
        wise_confident_note = `NOTE: The \"Wise Confident Response\" field was not found. There are several solutions for this:\n \
\t1. Enable the \"Write Confident Field\" checkbox in the RR section and refresh this page.\n \
\t2. Create it manually in Zendesk and refresh this page.\n \
\t3. Replace ${wise_confident_foreign_id} with a known field foreign_id in the following`;
      }

      return [
        {
          "id": 1,
          "description": `Review tickets en masse and bulk close tickets using the suggested macro where appropriate.\n\n${wise_confident_note}`,
          "foreign_attributes": {
            "title": "Wise Confident Response",
            "conditions": {
              "all": [
                {
                  "field": "status",
                  "operator": "less_than",
                  "value": "pending"
                },
                {
                  "field": "current_tags",
                  "operator": "includes",
                  "value": "wise-confident"
                }
              ],
              "any": []
            },
            "output": {
              "columns": [wise_confident_foreign_id, "subject", "created", "requester", "priority"],
              "group_by": wise_confident_foreign_id,
              "group_order": "desc",
              "sort_by": "created",
              "sort_order": "asc"
            }
          }
        },
        {
          "id": 2,
          "description": "Review potential auto-response (AR) candidates, before going live with AR.\n\nNOTE: Replace REPLACE_WITH_MACRO_FOREIGN_ID with an actual macro foreign id.",
          "foreign_attributes": {
            "title": "Auto-Responses Eligible for Review - Specific Macro",
            "conditions": {
              "all": [
                {
                  "field": "status",
                  "operator": "less_than",
                  "value": "closed"
                },
                {
                  "field": "current_tags",
                  "operator": "contains",
                  "value": "wise-auto-response-eligible-REPLACE_WITH_MACRO_FOREIGN_ID"
                }
              ]
            },
            "output": {
              "columns": ["subject", "requester", "created", "assignee"],
              "sort_by": "created"
            }
          }
        },
        {
          "id": 3,
          "description": "Track auto-responses for a specific macro.\n\nNOTE:\n\t1. Change the title to reflect the macro.\n\t2. Replace REPLACE_WITH_MACRO_FOREIGN_ID with an actual macro foreign id.",
          "foreign_attributes": {
            "title": "Auto-Responses Sent - Specific Macro",
            "conditions": {
              "all": [
                {
                  "field": "status",
                  "operator": "less_than",
                  "value": "closed"
                },
                {
                  "field": "current_tags",
                  "operator": "contains",
                  "value": "wise-auto-response-REPLACE_WITH_MACRO_FOREIGN_ID"
                }
              ]
            },
            "output": {
              "columns": ["subject", "requester", "created", "assignee"],
              "sort_by": "created"
            }
          }
        },
        {
          "id": 4,
          "description": "Track all auto-responses.",
          "foreign_attributes": {
            "title": "Auto-Responses Sent - All Macros",
            "conditions": {
              "all": [
                {
                  "field": "status",
                  "operator": "less_than",
                  "value": "closed"
                },
                {
                  "field": "current_tags",
                  "operator": "contains",
                  "value": "wise-auto-response"
                }
              ]
            },
            "output": {
              "columns": ["subject", "requester", "created", "assignee"],
              "sort_by": "created"
            }
          }
        }
      ];});
  },

  views(clientId) {
    return $http.get(`${config.apiBase}/api/v1/clients/${clientId}/views`).then(r => r.data);
  },

  fetchNow(clientId) {
    return $http.put(`${config.apiBase}/api/v1/clients/${clientId}/views`);
  },

  create(clientId, foreign_attributes) {
    return $http({
      method: "POST",
      url: `${config.apiBase}/api/v1/clients/${clientId}/views`,
      data: {
        "view": {
          "foreign_attributes": foreign_attributes
        }
      },
      headers: {
        "Content-Type": 'x-www-form-urlencoded'
      }
    });
  },

  update(clientId, id, foreign_attributes) {
    return $http({
      method: 'PUT',
      url: `${config.apiBase}/api/v1/clients/${clientId}/views/${id}`,
      data: {
        "view": {
          "foreign_attributes": foreign_attributes
        }
      },
      headers: {
        "Content-Type": 'x-www-form-urlencoded'
      }
    });
  },

  delete(clientId, id) {
    return $http.delete(`${config.apiBase}/api/v1/clients/${clientId}/views/${id}`);
  }
}));
