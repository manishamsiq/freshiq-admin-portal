/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

describe("FlashService:", function() {
  beforeEach(module("wiseSupport.flash"));

  beforeEach(inject(function(FlashService) {
    this.FlashService = FlashService;
    this.message = "This is an error message";
    return this.type = "error";
  })
  );


  it("defaults to not being visible", function() {
    return expect(this.FlashService.visible).toBeFalsy();
  });


  describe("flash()", function() {
    beforeEach(function() {
      return this.FlashService.flash(this.message, this.type);
    });

    it("sets this.message", function() {
      return expect(this.FlashService.message.toString()).toEqual(this.message);
    });

    it("sets this.type", function() {
      return expect(this.FlashService.type).toEqual(this.type);
    });

    return it("makes the flash visible", function() {
      return expect(this.FlashService.visible).toBeTruthy();
    });
  });


  return describe("close()", function() {
    beforeEach(function() {
      this.FlashService.flash(this.message, this.type);
      return this.FlashService.close();
    });

    return it("makes the flash invisible", function() {
      return expect(this.FlashService.visible).toBeFalsy();
    });
  });
});
