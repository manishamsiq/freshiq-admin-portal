/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.admin')
.service('AgentService', function($http, config) {

  let Agent;
  return {
    Agent: (Agent = class Agent {
      constructor(json) {
        _.extend(this, json);
        this.name = this.foreign_attributes.name;
        this.alias = this.foreign_attributes.alias;
        this.display_name = this.name + ` (ID: ${this.foreign_id})`;
        if (this.alias) {
          this.display_name += ` (Alias: ${this.alias})`;
        }
      }
    }),

    fetchAgents(clientId, with_tokens_only, search_string=null, page=null, per_page=null) {
      if (with_tokens_only == null) { with_tokens_only = true; }
      return $http.get(
        config.apiBase + `/api/v1/clients/${clientId}/agents`, {
        params: {
          with_tokens_only,
          search_string,
          page,
          per_page
        }
      }
      ).then(response => {
        response.data.agents = _.map(response.data.agents, agent => {
          return new this.Agent(agent);
        });
        return response.data;
      });
    }
  };
});
