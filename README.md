# AnswerIQ.com Confidential and Proprietary

# wise-support-frontend

The front-end of Wise Support 2.0.

## Setup

1. Install [Homebrew](http://brew.sh) (optional, but highly recommended).

2. Install [Node.js](https://nodejs.org) (these days, Node comes with [NPM](https://www.npmjs.org)) and PhantomJS. Assuming you have Homebrew installed:

   brew install node phantomjs

3. Install Ruby (> 2.0) and [Bundler](http://bundler.io). Mac OS comes with Ruby 2.0 these days, which is fine. Otherwise think about using [rbenv](https://github.com/sstephenson/rbenv) to manage Ruby versions. If you do stick with the built-in Ruby in Mac OS, keep in mind that you need to be root to install gems.

4. Install [gulp](http://gulpjs.com) and [bower](http://bower.io). Gulp is the build system we're using. Bower is a package manager for front-end libraries.

   npm install -g gulp bower

5. Install the rest of the prerequisites using their various dependency management systems:
   _Note: you might need to be root to run `bundle install` on your system_

         npm install
         bower install
         bundle install

6. Create your own dev config file and customize it to your needs:

   cp config/dev.coffee.sample config/dev.coffee

7. Run the development server and unit tests:

   gulp

8. Run the full test suite (note that the dev server needs to be running for E2E tests to pass):

   gulp test

## Alternative Setup

If the above setup doesn't work try this. Set up deploy environment on an ubuntu server:

    cd ~/src
    git clone git@bitbucket.org:ge-wise/wise-support-frontend.git
    curl -sL https://deb.nodesource.com/setup_6.x -o nodesource_setup.sh
    sudo bash nodesource_setup.sh
    sudo apt-get install -y nodejs build-essential ruby
    cd ~/src/wise-support-frontend/
    npm install phantomjs --save-dev
    sudo npm install -g gulp bower
    npm install
    bower install
    sudo gem install bundler
    bundle install
    npm rebuild node-sass
    cp config/dev.coffee.sample config/dev.coffee
    export AWS_ACCESS_KEY_ID="..."
    export AWS_SECRET_ACCESS_KEY="..."
    gulp
    # this gives some syntax warnings:
    gulp test

## Deployment

Deployment to an S3 bucket is done in one command. You only need to specify the destination bucket and the desired config file to use

    gulp deploy --bucket my-bucket-name.wise.io --config staging

AWS credentials are read from `~/.aws/credentials`.

## Managing packages

If there are changes to `bower.json`, make sure to run the following command before deployment

     bower install

Similarly, if there are changes to `Gemfile`, run

     bundle install

And if there are changes to `package.json`, run

     npm install

## Deployment Commands as of 5/14/2015

     AWS_PROFILE=wise-support gulp deploy --bucket support-development.smartassist.io --config staging
     AWS_PROFILE=wise-support gulp deploy --bucket support-staging.smartassist.io --config staging
     AWS_PROFILE=wise-support gulp deploy --bucket support.smartassist.io --config production

## AWS credentials

Make sure you have `~/.aws/credentials` and have these lines in it:

     [wise-support]
     aws_access_key_id=ANAMAZINGID12345
     aws_secret_access_key=superSEcRETamaZONKeysOMGsBBQChickenWow
     region=us-east-1

## Things to note

### Defining controllers

Make sure to `return this` from your controller definitions, so that Angular doesn't think you're returning a factory.

### Jasmine versions

The unit tests run vanilla Jasmine, currently version 2.1.

Note (June 20, 2017): The state of the e2e tests is unknown.

The e2e tests run [minijasminenode](https://github.com/juliemr/minijasminenode), and specifically they run a version which only supports Jasmine 1.x syntax.

### UI behaviour on Staging is different from behaiovour on Local OR Deploying from one machine causes a UI bug and deployment from another machine does not.

Reinstall everything:

    rm -rf node_modules bower_components build && npm install && bower install
