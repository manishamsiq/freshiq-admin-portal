/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

describe("LoginCtrl:", function() {
  beforeEach(module("wiseSupport.login"));

  beforeEach(inject(function($controller, $rootScope) {
    this.controllerService = $controller;
    return this.scope = $rootScope.$new();
  })
  );

  return describe("formSubmit()", function() {
    beforeEach(inject(function($state, UserService, FlashService, $q) {
      this.$state = $state;
      this.UserService = UserService;
      this.FlashService = FlashService;
      spyOn(this.$state, "go");
      spyOn(this.FlashService, "flash");
      spyOn(this.UserService, "login").and.callFake(() => {
        let response;
        const deferred = $q.defer();
        if (this.succeed) {
          response = new this.UserService.User();
          deferred.resolve(response);
        } else {
          response = {data: 'Login failed'};
          deferred.reject(response);
        }
        return deferred.promise;
      });

      return this.controller = this.controllerService("LoginCtrl", {
        $scope: this.scope,
        $state: this.$state,
        UserService: this.UserService,
        FlashService: this.FlashService
      }
      );
    })
    );


    describe("a successful login", function() {
      beforeEach(function() {
        this.succeed = true;
        this.controller.formSubmit();
        return this.scope.$digest();
      });

      it("calls the login service", function() {
        return expect(this.UserService.login).toHaveBeenCalled();
      });

      it("redirects to the routing", function() {
        return expect(this.$state.go).toHaveBeenCalledWith("routing", {});
      });

      return it("doesn't show an error to the user", function() {
        return expect(this.FlashService.flash).not.toHaveBeenCalled();
      });
    });

    return describe("a failed login", function() {
      beforeEach(function() {
        this.succeed = false;
        this.controller.formSubmit();
        return this.scope.$digest();
      });

      it("calls the login service", function() {
        return expect(this.UserService.login).toHaveBeenCalled();
      });

      it("doesn't redirect the user", function() {
        return expect(this.$state.go).not.toHaveBeenCalledWith();
      });

      it("flashes an error", function() {
        return expect(this.FlashService.flash)
          .toHaveBeenCalledWith("Login failed", "error");
      });

      return it("clears the password field after a failed login", function() {
        return expect(this.controller.password).toBeFalsy();
      });
    });
  });
});
