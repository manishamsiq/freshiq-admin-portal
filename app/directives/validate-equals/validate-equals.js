/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.validateEquals', [])
  .directive('validateEquals', () => ({
  restrict: 'A',
  require: 'ngModel',
  scope: { validateEquals: '=' },

  link(scope, element, attrs, ngModelCtrl) {
    return ngModelCtrl.$validators.validateEquals = value => scope.validateEquals === value;
  }
}));
