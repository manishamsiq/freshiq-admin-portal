// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.ticketFields', [
  'ngDialog',
  'ui.router',
  'wiseSupport.config',
  'wiseSupport.flash'
]);
