/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.homepage', [
  'ui.router'
]);

angular.module('wiseSupport.homepage').config($stateProvider => $stateProvider
  .state('homepage', {
    url: '/',
    resolve: {
      user(UserService) {
        return UserService.me();
      },

      route($state, user, ForcedRouteService) {
        if (user) {
          return $state.go(ForcedRouteService.defaultRouteForUser(user));
        } else {
          return $state.go('login');
        }
      }
    }
  }
));
