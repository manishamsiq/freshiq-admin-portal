/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

describe("UserService:", function() {
  beforeEach(module("wiseSupport.user"));

  beforeEach(inject(function(UserService,
                     $httpBackend,
                     config) {
    this.UserService = UserService;
    this.$httpBackend = $httpBackend;
    this.config = config;
    this.baseUrl = `${this.config.apiBase}/api/v1/auth`;
    this.email = "unit-test@example.com";
    this.password = "unsecure";
    return this.token = "thisIsAToken";
  })
  );


  describe("login()", function() {
    beforeEach(function() {
      this.$httpBackend.whenPOST(this.baseUrl).respond();
      return this.result = this.UserService.login(this.email, this.password);
    });

    afterEach(function() { return this.$httpBackend.flush(); });

    it("POSTs to /auth", function() {
      return this.$httpBackend.expectPOST(this.baseUrl, {
        email: this.email,
        password: this.password
      });
    });

    return it("returns a promise", function() {
      return expect(this.result.then).toBeDefined();
    });
  });


  describe("logout()", function() {
    beforeEach(function() {
      this.$httpBackend.whenDELETE(this.baseUrl).respond();
      return this.result = this.UserService.logout();
    });

    afterEach(function() { return this.$httpBackend.flush(); });

    it("DELETEs to /auth", function() {
      return this.$httpBackend.expectDELETE(this.baseUrl);
    });

    return it("returns a promise", function() {
      return expect(this.result.then).toBeDefined();
    });
  });


  describe("sendPasswordResetEmail()", function() {
    beforeEach(function() {
      this.url = this.baseUrl + "/reset-password";
      this.$httpBackend.whenPOST(this.url).respond();
      return this.result = this.UserService.sendPasswordResetEmail(this.email);
    });

    afterEach(function() { return this.$httpBackend.flush(); });

    it("POSTs to /auth/reset-password", function() {
      return this.$httpBackend.expectPOST(this.url, {email: this.email});
    });

    return it("returns a promise", function() {
      return expect(this.result.then).toBeDefined();
    });
  });


  describe("resetPassword()", function() {
    beforeEach(function() {
      this.url = this.baseUrl + "/reset-password";
      this.$httpBackend.whenPUT(this.url).respond();
      return this.result = this.UserService.resetPassword(this.token, this.password);
    });

    afterEach(function() { return this.$httpBackend.flush(); });

    it("PUTs to /auth/reset-password", function() {
      return this.$httpBackend.expectPUT(this.url, {token: this.token, password: this.password});
    });

    return it("returns a promise", function() {
      return expect(this.result.then).toBeDefined();
    });
  });


  return describe("me()", function() {
    beforeEach(function() {
      this.url = this.baseUrl + "/me";
      this.me = {
        client_id: 43,
        email: this.email,
        given_name: "Test",
        surname: "User"
      };

      this.$httpBackend.whenGET(this.url).respond(this.me);
      return this.result = this.UserService.me(this.email);
    });

    afterEach(function() { return this.$httpBackend.flush(); });

    it("GETs to /auth/me", function() {
      return this.$httpBackend.expectGET(this.url);
    });

    it("returns a User object", function() {
      const expected = new this.UserService.User(this.me);
      return this.result.then(response => expect(response.toString()).toEqual(expected.toString()));
    });

    return it("returns a promise", function() {
      return expect(this.result.then).toBeDefined();
    });
  });
});
