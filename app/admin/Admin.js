/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.admin', [
  'ui.router',
  'wiseSupport.validateEquals',
  'wiseSupport.dropdown'
]);

angular.module('wiseSupport.admin').config($stateProvider => $stateProvider
  .state('admin', {
    url: "/admin",
    abstract: true,
    templateUrl: "/admin/admin.html",
    resolve: {
      user(UserService) {
        return UserService.me();
      },

      route(user, ForcedRouteService) {
        return ForcedRouteService.requiresAdmin(user);
      }
    }
  }).state('admin.clients', {
    url: "/clients",
    templateUrl: '/admin/client_list.html',
    controller: 'ClientListCtrl as clients'
  }).state('admin.client', {
    url: "/clients/:clientId",
    templateUrl: '/admin/client.html',
    controller: 'ClientCtrl as client',
    abstract: true,
    params: {
      clientId: {
        squash: true
      }
    },
    resolve: {
      client($stateParams, ClientService) {
        return ClientService.get($stateParams.clientId);
      }
    }
  }).state('admin.client.fields', {
    url: '/fields',
    controller: 'FieldsAdminCtrl as controller',
    templateUrl: '/admin/client/fields.html'
  }).state('admin.client.macro-models', {
    url: '/macro-models',
    controller: 'MacroModelsAdminCtrl as controller',
    templateUrl: '/admin/client/macro-models.html'
  }).state('admin.client.users', {
    url: '/users',
    templateUrl: '/admin/client/users.html',
    controller: 'UsersAdminCtrl as controller'
  }).state('admin.client.triggers', {
    url: "/triggers",
    templateUrl: "/triggers/triggers.html",
    controller: "TriggerCtrl as controller",
    params: {
      perPage: {
        value: 15,
        squash: true,
        type: { name: "int"
      }
      },
      page: {
        value: 0,
        squash: true,
        type: { name: "int"
      }
      },
      search: {
        value: "",
        squash: true
      }
    }
  }).state('admin.client.views', {
    url: "/views",
    templateUrl: "/views/index.html",
    controller: "ViewCtrl as controller"
  }).state('admin.client.misc', {
    url: '/misc',
    templateUrl: '/admin/client/misc.html',
    controller: 'MiscAdminCtrl as miscCtrl'
  }).state('admin.client.unified-widget', {
    url: '/unified-widget',
    templateUrl: '/admin/client/unified_widget/show.html',
    controller: 'UnifiedWidgetCtrl as controller'
  }).state('admin.client.unified-widget-documentation', {
    url: '/unified-widget/docs',
    templateUrl: '/admin/client/unified_widget/documentation.html',
    controller: 'UnifiedWidgetDocumentationCtrl as controller'
  }
));
