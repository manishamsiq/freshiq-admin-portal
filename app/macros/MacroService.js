/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.macros')
.service('MacroService', function($http, config) {

  let EmailTemplate, Macro;
  return {
    macros: undefined,

    Macro: (Macro = class Macro {
      constructor(json) {
        _.extend(this, json);
        this.date = Date.parse(this.zendesk_created_at);
        this.values = this.value_dict;
      }

      getBody() {
        const comment_action = _.find(this.actions, {field: "comment_value"});
        return (comment_action != null ? comment_action.value : undefined) || "No comment body";
      }

      truncated_title(length, tail) {
        if (tail == null) { tail = '...'; }
        const max_title_length = length - tail.length;
        if (this.title.length > max_title_length) {
          return this.title.slice(0, +max_title_length + 1 || undefined) + tail;
        } else {
          return this.title;
        }
      }
    }),

    EmailTemplate: (EmailTemplate = class EmailTemplate {
      constructor(json) {
        _.extend(this, json);
        this.title = this.name;
      }
    }),

    fetchMacros(clientId) {
      return $http.get(`${config.apiBase}/api/v1/clients/${clientId}/macros`).then(response => {
        return this.macros = _.map(response.data.macros, macro => {
          return new this.Macro(macro);
        });
      });
    },

    fetchMacro(clientId, macroForeignId) {
      const on_success = response => {
        return new this.Macro(response.data);
      };
      const on_error = response => {
        return {'foreign_id': macroForeignId};
      };
      return $http.get(`${config.apiBase}/api/v1/clients/${clientId}/macros/${macroForeignId}`).then(on_success, on_error);
    },

    getMacro(macroId) {
      return _.find(this.macros, {id: macroId});
    },

    getMacroByZendeskId(macroZendeskId) {
      return _.find(this.macros, {zendesk_id: macroZendeskId});
    },

    fetchEmailTemplate(clientId, emailTemplateForeignId) {
      const on_success = response => {
        return new this.EmailTemplate(response.data);
      };
      const on_error = response => {
        return {'foreign_id': emailTemplateForeignId};
      };
      return $http.get(`${config.apiBase}/api/v1/clients/${clientId}/email-templates/${emailTemplateForeignId}`).then(on_success, on_error);
    },

    previewMacro(clientId, data) {
      return $http({
        method: 'POST',
        url: `${config.apiBase}/api/v1/clients/${clientId}/response-preview`,
        data
      });
    }
  };
});
