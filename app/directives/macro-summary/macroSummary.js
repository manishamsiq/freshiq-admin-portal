/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.macroSummary', [])
  .directive('macroSummary', () => ({
  restrict: 'E',

  scope: {
    macroZendeskId: '='
  },

  templateUrl: "/directives/macro-summary/macro_summary.html",

  link(scope, element) {
    return element.addClass("tooltip-item");
  },

  controller($scope, MacroService) {
    return $scope.macro = MacroService.getMacroByZendeskId($scope.macroZendeskId);
  }
}));
