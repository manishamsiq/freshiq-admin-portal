/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.response', [
  'ui.router'
]);

angular.module('wiseSupport.response').config($stateProvider => $stateProvider
  .state('response', {
    parent: 'client',
    url: "/response",
    templateUrl: '/response/index.html',
    controller: 'ResponseCtrl as controller'
  }
));
