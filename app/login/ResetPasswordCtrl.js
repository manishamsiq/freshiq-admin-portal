/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.login')
.controller('ResetPasswordCtrl',
function(UserService, FlashService, $stateParams) {
  this.formSubmit = () => {
    const success = () => {
      FlashService.close();
      return this.sent = true;
    };

    const error = function(response) {
      const message = (response.data != null ? response.data.message : undefined) || "Reset failed.";
      return FlashService.flash(message, "error");
    };

    return UserService.resetPassword($stateParams.token, this.password)
      .then(success, error);
  };

  return this;
});
