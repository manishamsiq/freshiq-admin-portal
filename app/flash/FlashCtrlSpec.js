/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

describe("FlashCtrl:", function() {
  beforeEach(module("wiseSupport.login"));

  beforeEach(inject(function($controller, $rootScope) {
    this.controllerService = $controller;
    return this.scope = $rootScope.$new();
  })
  );


  describe("close()", function() {
    beforeEach(inject(function(FlashService) {
      this.FlashService = FlashService;
      spyOn(this.FlashService, "close");

      this.controller = this.controllerService("FlashCtrl", {
        $scope: this.scope,
        FlashService: this.FlashService
      }
      );

      return this.controller.close();
    })
    );

    return it("asks the FlashService to close the flash", function() {
      return expect(this.FlashService.close).toHaveBeenCalled();
    });
  });


  return describe("$stateParams error message", function() {
    beforeEach(inject(function(FlashService) {
      this.FlashService = FlashService;
      spyOn(this.FlashService, "flash");

      this.message = "This is an error message";

      return this.controller = this.controllerService("FlashCtrl", {
        $scope: this.scope,
        FlashService: this.FlashService,
        $stateParams: {
          errorMessage: this.message
        }
      }
      );
    })
    );

    return it("causes the flash to show", function() {
      return expect(this.FlashService.flash).toHaveBeenCalledWith(this.message, "error");
    });
  });
});
