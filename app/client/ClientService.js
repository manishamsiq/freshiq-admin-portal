/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.client')
.service('ClientService', function($http, config) {

  let Client;
  return {
    Client: (Client = class Client {
      constructor(json) {
        _.extend(this, json);
        const created_at = Date.parse(this.created_at);
      }
    }),

    url: config.apiBase + "/api/v1/clients",

    get(id) {
      if (!id) {
        return null;
      }

      return $http.get(this.url + "/" + id).then(response => {
        if (response != null) {
          return new this.Client(response.data);
        }
      });
    },

    get_all(active_only, inactive_only) {
      if (active_only == null) { active_only = false; }
      if (inactive_only == null) { inactive_only = false; }
      const url = `${this.url}?active_only=${active_only}&inactive_only=${inactive_only}`;
      return $http.get(url).then(response => {
        return _.map(response.data, data => {
          return new this.Client(data);
        });
      });
    },

    patch(clientId, params) {
      return $http.patch(this.url + `/${clientId}`, params).then(function(response) {
        return new this.Client(response.data);
      });
    },

    delete(clientId) {
      return $http.delete(config.apiBase + `/api/v1/clients/${clientId}`);
    }
  };
});
