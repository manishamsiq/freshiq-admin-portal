/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.signup', [
  'ui.router',
  'wiseSupport.config',
  'wiseSupport.flash',
  'teljs'
]);

angular.module('wiseSupport.signup').config($stateProvider => $stateProvider
  .state('signup', {
    url: '/signup',
    templateUrl: '/signup/signup.html',
    abstract: true
  }).state('signup.choose-support-system', {
    url: '/choose-support-system',
    templateUrl: '/signup/choose_support_system.html'
  }).state('signup.contact', {
    url: '/contact',
    params: {
      formGUID: {
        value: "f32f02fc-440b-492b-beb0-a0f02333a566",
        squash: true
      }
    },
    templateUrl: '/signup/contact/contact.html',
    controller: 'SignupContactCtrl as contact'
  }).state('signup.connect-to-zendesk', {
    url: '/zendesk/connect?errorMessage',
    templateUrl: '/signup/zendesk/connect-to-zendesk.html',
    controller: 'ConnectToZendeskCtrl as client',
    params: {
      subdomain: {
        value: null,
        squash: true
      }
    }
  }).state('signup.connect-to-servicecloud', {
    url: '/servicecloud/connect?errorMessage',
    templateUrl: '/signup/servicecloud/connect-to-servicecloud.html',
    controller: 'ConnectToServicecloudCtrl as client'
  }).state('signup.new-user', {
    url: '/new-user',
    templateUrl: '/signup/new_user.html',
    controller: 'NewUserCtrl as user',
    resolve: {
      user(UserService) {
        return UserService.me();
      },

      route(user, $state) {
        // If there's already a user, skip this step
        if (user) {
          return $state.go('routing');
        }
      },

      me(UserService, route) {
        return UserService.halfSignedUpMe();
      }
    }
  }).state('signup.finalize', {
    url: '/finalize',
    templateUrl: '/signup/finalize.html',
    controller: 'FinalizeCtrl as controller',
    resolve: {
      user(UserService) {
        return UserService.me();
      },

      client(user, ClientService) {
        return ClientService.get(user.client_id);
      }
    }
  }).state('oauth_login', {
    url: '/oauth/login',
    controller($state) {
      return $state.go('signup.choose-support-system');
    }
  }
));
