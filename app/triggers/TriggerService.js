/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.triggers')
.service('TriggerService', function($http, config) {

  let Trigger;
  return {
    Trigger: (Trigger = class Trigger {
      constructor(json) {
        _.extend(this, json);
        this.anyAndAll = this.conditions.any.length && this.conditions.all.length;
      }
    }),

    url(clientId) {
      return config.apiBase + `/api/v1/clients/${clientId}/triggers`;
    },

    triggers(clientId) {
      return $http.get(this.url(clientId)).then(response => {
        const {
          data
        } = response;
        data.triggers = _.map(response.data.triggers, trigger => {
          return new this.Trigger(trigger);
        });
        return data;
      });
    }
  };
});
