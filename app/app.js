/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

// Declare the main module and dependencies
angular.module('wiseSupport', [
  // External modules
  'ui.router',
  'angular-ladda',
  'angular-loading-bar',
  'angularSpinner',
  'ngDialog',
  'sticky',
  'ngAnimate',
  'ngCsv',

  // Our modules
  'wiseSupport.admin',
  'wiseSupport.client',
  'wiseSupport.config',
  'wiseSupport.flash',
  'wiseSupport.homepage',
  'wiseSupport.login',
  'wiseSupport.macroModels',
  'wiseSupport.macros',
  'wiseSupport.macroSuggestions',
  'wiseSupport.oauth',
  'wiseSupport.popup',
  'wiseSupport.response',
  'wiseSupport.routing',
  'wiseSupport.signup',
  'wiseSupport.ticketFields',
  'wiseSupport.triggers',
  'wiseSupport.user',
  'wiseSupport.views'
]);


angular.module('wiseSupport').config($locationProvider => $locationProvider.html5Mode({enabled: true, requireBase: false}));

angular.module('wiseSupport').config($urlRouterProvider => $urlRouterProvider.otherwise('/'));

angular.module('wiseSupport').config(function($httpProvider) {
  $httpProvider.defaults.withCredentials = true;
  return $httpProvider.defaults.headers.delete = {'Content-Type': 'application/json'};});

angular.module('wiseSupport').config($provide => $provide.decorator('$state', function($delegate, $rootScope) {
  $rootScope.$on('$stateChangeStart', function(event, state, params) {
    $delegate.next = state;
    return $delegate.nextParams = params;
  });
  return $delegate;
}));

angular.module('wiseSupport').config(function(cfpLoadingBarProvider) {
  cfpLoadingBarProvider.includeSpinner = false;
  return cfpLoadingBarProvider.latencyThreshold = 250;
});

// We have to have $state here, to avoid this bug:
// https://github.com/angular-ui/ui-router/issues/679#issuecomment-31116942
angular.module('wiseSupport').run(function($state, $rootScope) {
  // UI Router silently swallows errors on resolve. This exposes them.
  $rootScope.$on('$stateChangeError',
  function(event, toState, toParams, fromState, fromParams, error) {
    throw error;
  });

  //listening for logout event from the other tabs and moving to login page
  return window.addEventListener('storage', function(event) {
    if (event.key === 'logout-event') {
      return $state.go('login');
    }
  });
});

// Tell mixpanel to register all state changes
angular.module('wiseSupport').run(function($state, $rootScope, config) {
  if (window.mixpanel) {
    mixpanel.init(config.mixpanelToken);

    return $rootScope.$on('$stateChangeStart', (event, toState) => mixpanel.track(`Visiting ${toState.name}`));
  }
});
