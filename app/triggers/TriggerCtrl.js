/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.triggers')
.controller('TriggerCtrl', function($state, $stateParams, client, $filter, TriggerService) {
  this.client = client;
  this.page = $stateParams.page;
  this.offset = this.page * $stateParams.perPage;
  this.search = $stateParams.search;
  this.count = null;
  this.triggers = null;
  this.loading = true;

  this.hasPreviousPage = function() {
    return this.page > 0;
  };

  this.hasNextPage = function() {
    return this.count > (this.offset + this.triggers.length);
  };

  this.nextPage = function() {
    return $state.go(".", {page: this.page + 1});
  };

  this.previousPage = function() {
    return $state.go(".", {page: this.page - 1});
  };

  this.doSearch = function() {
    return $state.go(".", {search: this.search});
  };

  this.setup = () => {
    return TriggerService.triggers(this.client.id).then(triggers => {
      triggers = $filter('filter')(triggers.triggers, this.search);
      this.count = triggers.length;
      this.triggers = triggers.slice(this.offset, this.offset + $stateParams.perPage);
      return this.loading = false;
    });
  };

  this.setup();

  return this;
});
