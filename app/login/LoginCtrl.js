/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.login')
.controller('LoginCtrl',
function($state, $stateParams, UserService, FlashService, ForcedRouteService) {
  this.formSubmit = () => {
    const success = function(user) {
      FlashService.close();
      const destination = $stateParams.destination ||
                    ForcedRouteService.defaultRouteForUser(user);
      const params = JSON.parse($stateParams.destinationParams || "{}");
      return $state.go(destination, params);
    };

    const error = response => {
      this.password = "";
      return FlashService.flash(response.data, "error");
    };

    return UserService.login(this.email, this.password).then(success, error);
  };

  return this;
});
