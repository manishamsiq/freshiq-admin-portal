/*
 * decaffeinate suggestions:
 * DS101: Remove unnecessary use of Array.from
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport')
.factory('ForcedRouteService', function($state) {

  const goWithDestination = (state, destination) => $state.go(state, {
    destination: $state.next.name,
    destinationParams: JSON.stringify($state.nextParams)
  }
  );

  this.requiresOnboarded = (user, client) => {
    if (!client || !user) {
      goWithDestination('signup.login');
    }

    if (Array.from(user.roles).includes('admin')) {
      return;
    }

    if (!client.onboarded) {
      return goWithDestination('signup.finalize');
    }
  };

  this.requiresAdmin = function(user) {
    if (!user || !Array.from(user.roles).includes('admin')) {
      return goWithDestination('login');
    }
  };

  this.defaultRouteForUser = function(user) {
    if (user.isAdmin()) {
      return 'admin.clients';
    } else {
      return 'routing';
    }
  };

  return this;
});
