/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.percentBar', [])
  .directive('percentBar', () => ({
  restrict: 'E',

  scope: {
    percent: '@',
    label: '@'
  },

  templateUrl: "/directives/percent_bar/percent_bar.html"
}));
