/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

describe("ClientService:", function() {
  beforeEach(module("wiseSupport.client"));

  beforeEach(inject(function(ClientService, $httpBackend, config) {
    this.ClientService = ClientService;
    this.$httpBackend = $httpBackend;
    this.config = config;
    this.baseUrl = `${this.config.apiBase}/api/v1/clients`;
    this.clientId = 42;
    return this.clientResponse = {
      created_at: "2015-01-28T10:49:35.887801-05:00",
      has_enabled_ticket_fields: 3,
      id: 1,
      subdomain: "wiseiodev"
    };
  })
  );

  return describe("get()", function() {
    beforeEach(function() {
      this.url = this.baseUrl + "/" + this.clientId;
      this.$httpBackend.whenGET(this.url).respond(this.clientResponse);
      return this.result = this.ClientService.get(this.clientId);
    });

    afterEach(function() { return this.$httpBackend.flush(); });

    it("GETs to /clients", function() {
      return this.$httpBackend.expectGET(this.url);
    });

    it("unwraps response data", function() {
      const client = this.ClientService.Client(this.clientResponse);
      return this.result.then(response => {
        return expect(response.toString()).toEqual(this.clientResponse.toString());
      });
    });

    return it("returns a promise", function() {
      return expect(this.result.then).toBeDefined();
    });
  });
});
