/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.admin')
.controller('ClientCtrl', function(client, $state) {

  this.client = client;
  this.name = this.client.name;

  this.stateIsActive = state => state === _.last($state.current.name.split("."));

  return this;
});
