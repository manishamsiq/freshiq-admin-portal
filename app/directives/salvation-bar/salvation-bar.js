/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.salvationBar', [])
  .directive('salvationBar', $timeout => ({
  restrict: 'E',

  scope: {
    visible: '='
  },

  templateUrl: "/directives/salvation-bar/salvation-bar.html",

  link(scope, element, attrs, ngModelCtrl) {
    scope.width = 100;
    return scope.$watch("visible", function(visible) {
      if (visible) {
        scope.width = 3;

        $timeout(() => scope.width = 100);

        return $timeout(() => scope.visible = false
        , 500);
      }
    });
  }
}));
