/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.predictionStatus', [])
  .directive('predictionStatus', () => ({
  restrict: 'E',

  scope: {
    ticket: '=',
    field: '=',
    showHeader: "@"
  },

  templateUrl: "/directives/prediction-status/prediction_status.html",

  link(scope, element) {
    element.addClass("tooltip-item");
    if (!scope.ticket.isConfident(scope.field)) {
      return element.addClass("not-confident");
    }
  }
}));
