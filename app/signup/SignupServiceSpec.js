/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

describe("SignupService:", function() {
  beforeEach(module("wiseSupport.signup"));

  beforeEach(inject(function(SignupService, $httpBackend, config) {
    this.SignupService = SignupService;
    this.$httpBackend = $httpBackend;
    this.config = config;
    return this.baseUrl = `${this.config.apiBase}/api/v1`;
  })
  );


  describe("newUser()", function() {
    beforeEach(function() {
      this.url = this.baseUrl + "/users";
      this.givenName = "Test";
      this.surname = "User";
      this.email = "unit-test@example.com";
      this.password = "unsecure";

      this.$httpBackend.whenPOST(this.url).respond();

      return this.result = this.SignupService
        .newUser(this.givenName, this.surname, this.email, this.password);
    });

    afterEach(function() { return this.$httpBackend.flush(); });

    it("POSTs to /users", function() {
      return this.$httpBackend.expectPOST(this.url, {
        given_name: this.givenName,
        surname: this.surname,
        email: this.email,
        password: this.password
      }
      );
    });

    return it("returns a promise", function() {
      return expect(this.result.then).toBeDefined();
    });
  });


  describe("zendeskOauthURL()", function() {
    beforeEach(function() {
      this.result = this.SignupService.zendeskOauthURL("wiseiodev",
                                               "/success", "/error");

      return this.expected =
        `${this.config.apiBase}/oauth/zendesk?subdomain=wiseiodev` +
        "&redirect_url=http%3A%2F%2Fserver%3A80%2Fsuccess" +
        "&error_url=http%3A%2F%2Fserver%3A80%2Ferror";
    });

    return it("returns a URL encoded OAuth URL", function() {
      return expect(this.result).toBe(this.expected);
    });
  });


  return describe("connectWithAPIKey", function() {
    beforeEach(function() {
      this.url = this.baseUrl + "/clients";
      this.foreign_id = "wiseiodev";
      this.email = "unit-test@example.com";
      this.apiToken = "this_is_a_token";

      this.$httpBackend.whenPOST(this.url).respond();

      return this.result = this.SignupService.connectWithAPIKey(this.foreign_id, this.email, this.apiToken);
    });

    afterEach(function() { return this.$httpBackend.flush(); });

    it("POSTs to /clients", function() {
      return this.$httpBackend.expectPOST(this.url, {
        foreign_id: this.foreign_id,
        api_email: this.email,
        api_token: this.apiToken
      }
      );
    });

    return it("returns a promise", function() {
      return expect(this.result.then).toBeDefined();
    });
  });
});
