/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

describe("NewUserCtrl:", function() {
  beforeEach(module("wiseSupport.signup"));

  beforeEach(inject(function($controller, $rootScope) {
    this.controllerService = $controller;
    return this.scope = $rootScope.$new();
  })
  );

  return describe("formSubmit()", function() {
    beforeEach(inject(function($state, SignupService, FlashService, $q) {
      this.$state = $state;
      this.SignupService = SignupService;
      this.FlashService = FlashService;
      spyOn(this.$state, "go");
      spyOn(this.FlashService, "flash");
      spyOn(this.SignupService, "newUser").and.callFake(() => {
        const deferred = $q.defer();
        if (this.succeed) {
          deferred.resolve();
        } else {
          deferred.reject();
        }
        return deferred.promise;
      });

      return this.controller = this.controllerService("NewUserCtrl", {
        $scope: this.scope,
        $state: this.$state,
        SignupService: this.SignupService,
        FlashService: this.FlashService,
        me: {
          givenName: "Freddy",
          surname: "Bickens",
          email: "Freddy@bicke.ns"
        }
      }
      );
    })
    );

    describe("a successful signup", function() {
      beforeEach(function() {
        this.succeed = true;
        this.controller.formSubmit();
        return this.scope.$digest();
      });

      it("calls the signup service", function() {
        return expect(this.SignupService.newUser).toHaveBeenCalled();
      });

      it("redirects to the routing", function() {
        return expect(this.$state.go).toHaveBeenCalledWith("routing");
      });

      return it("doesn't show an error to the user", function() {
        return expect(this.FlashService.flash).not.toHaveBeenCalled();
      });
    });

    return describe("an unsuccessful signup", function() {
      beforeEach(function() {
        this.succeed = false;
        this.controller.formSubmit();
        return this.scope.$digest();
      });

      it("calls the signup service", function() {
        return expect(this.SignupService.newUser).toHaveBeenCalled();
      });

      it("doesn't redirect the user", function() {
        return expect(this.$state.go).not.toHaveBeenCalled();
      });

      return it("flashes an error", function() {
        return expect(this.FlashService.flash)
          .toHaveBeenCalledWith("Signup failed", "error");
      });
    });
  });
});
