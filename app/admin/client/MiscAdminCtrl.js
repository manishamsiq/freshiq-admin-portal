/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.admin')
.controller('MiscAdminCtrl',
function(client, ClientService, FlashService, config, $state, $timeout, $window) {

  // Used to display the CURL url
  this.client = client;
  this.apiBase = config.apiBase;

  this.name = this.client.name;

  this.patchActive = () => {
    return ClientService.patch(this.client.id, {active: this.client.active}).then(newClient => {
      return this.client = newClient;
    });
  };

  this.patchOnboarded = () => {
    return ClientService.patch(this.client.id, {onboarded: this.client.onboarded}).then(newClient => {
      return this.client = newClient;
    });
  };

  this.updateClientSettings = function() {
    if (this.client.max_tickets_age_keep_in_months < 2) {
      return alert("We need to keep tickets that are less than 2 months old");
    } else {
      this.saving = true;
      const on_success = response => {
        return $timeout(() => {
          return this.saving = false;
        }
        , 750);
      };
      const on_error = response => {
        FlashService.flash(response.data.error, "error");
        return $timeout(() => {
          return this.saving = false;
        }
        , 750);
      };
      return ClientService.patch(this.client.id, {
        max_tickets: this.client.max_tickets,
        max_tickets_age_keep_in_months: this.client.max_tickets_age_keep_in_months,
        fetch_period: this.client.fetch_period,
        fetch_attachments_enabled: this.client.fetch_attachments_enabled,
        fetch_csats_enabled: this.client.fetch_csats_enabled,
        fetch_csats_filter: this.client.fetch_csats_filter,
        dmc_enabled: this.client.dmc_enabled,
        dmc_substitutions_template: this.client.dmc_substitutions_template
      }).then(on_success, on_error);
    }
  };

  this.deleteClient = function() {
    if (window.confirm("Delete client permanently?")) {
      return ClientService.delete(this.client.id).then(response => {
        FlashService.flash(`${this.name} is being deleted and will be removed shortly.`, "success");
        return $state.go("admin.clients");
      });
    }
  };

  return this;
});
