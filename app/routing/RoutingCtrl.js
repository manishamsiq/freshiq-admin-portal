/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.routing')
.controller('RoutingCtrl',
function(client, TicketFieldService, $timeout) {

  this.client = client;
  this.loading = true;
  this.fields = null;
  this.open = null;
  this.saved = false;

  this.setEnabled = function(field) {
    return TicketFieldService.alterField(this.client.id, field.id,
      {enabled: field.enabled})
    .then(newField => {
      this.saved = true;
      return field.constructor(newField.original);
    });
  };

  this.setPublished = function(field) {
    return TicketFieldService.alterField(this.client.id, field.id,
      {publish_predictions: field.publish_predictions})
    .then(newField => {
      this.saved = true;
      return field.constructor(newField.original);
    });
  };

  this.setPublishToField = function(field) {
    return TicketFieldService.alterField(this.client.id, field.id,
      {publish_to_field: field.publish_to_field})
    .then(newField => {
      this.saved = true;
      return field.constructor(newField.original);
    });
  };

  this.setup = () => {
    this.loading = true;
    return TicketFieldService.enabledTicketFields(this.client.id).then(fields => {
      this.fields = _.filter(fields, f => f.enabled);
      if (this.fields) {
        this.open = this.fields[0];
      }
      return this.loading = false;
    });
  };

  // delay 50 ms so "UserService.me()" doesn't timeout
  $timeout(this.setup, 50);

  return this;
});
