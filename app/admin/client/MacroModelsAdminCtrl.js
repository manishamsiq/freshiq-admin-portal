/*
 * decaffeinate suggestions:
 * DS101: Remove unnecessary use of Array.from
 * DS102: Remove unnecessary code created because of implicit returns
 * DS205: Consider reworking code to avoid use of IIFEs
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.admin')
.controller('MacroModelsAdminCtrl',
function(client, MacroModelService, FlashService, PopupService, AgentService, FromAddressesService, $filter, $q) {
  this.client = client;
  this.models = null;
  this.agent_responses = null;
  this.agents = null;
  this.loading = true;

  this.setEnabled = function(model) {
    return MacroModelService.alterModel(this.client, model.id,
      {enabled: model.enabled})
    .then(newModel => {
      this.saved = true;
      model.constructor(newModel.original);
      MacroModelService.fetchModelConfig(this.client, model).then(function() {});
      if (model.enabled) { return FlashService.flash("Please click Rebuild Now to start building " + model.name, "success"); }
    });
  };

  this.setPublished = function(model) {
    let message;
    if (model.publish_predictions) {
      if (this.client.data_source.name === 'zendesk') {
        message = "Enabling publishing will create " + model.confident_field_name +
        " field in Zendesk account and start publishing tags on tickets. ";
      }
      if (this.client.data_source.name === 'servicecloud') {
        message = "Enabling publishing will start writing predictions to wisespt__WiseResponse__c field on WisePrediction objects. ";
      }
    } else {
      if (this.client.data_source.name === 'zendesk') {
        message = "Disabling publishing will stop publishing tags on tickets. ";
      }
      if (this.client.data_source.name === 'servicecloud') {
        message = "Disabling publishing will stop writing predictions to wisespt__WiseResponse__c field on WisePrediction objects. ";
      }
    }

    return PopupService.popup(message + "Are you sure you want to do this?")
    .then(() => {
      const on_success = newModel => {
        this.saved = true;
        model.constructor(newModel.original);
        if (model.publish_predictions) {
          return FlashService.flash("Enabled publishing for " + model.name, "success");
        } else {
          return FlashService.flash("Disabled publishing for " + model.name, "success");
        }
      };
      const on_error = response => {
        return FlashService.flash(response.data.error, "error");
      };

      return MacroModelService.alterModel(this.client, model.id,
        {publish_predictions: model.publish_predictions})
      .then(on_success, on_error);
    });
  };

  this.saveSchedule = function(model) {
    return MacroModelService.alterModel(this.client, model.id,
      {rebuild_schedule: model.rebuild_schedule})
    .then(newModel => {
      this.saved = true;
      model.constructor(newModel.original);
      return FlashService.flash("Changed rebuild schedule for " + model.name, "success");
    });
  };

  this.rebuild = function(model) {
    return MacroModelService.rebuild(this.client, model).then(() => FlashService.flash("Rebuilding " + model.name, "success"));
  };

  this.setProperty = (model, name, value) => {
    const on_success = newModel => {
      this.saved = true;
      model.constructor(newModel.original);
      return FlashService.flash(`Updated \"${name}\" to \"${value}\" for ${model.name}`, "success");
    };
    const on_error = response => {
      return FlashService.flash(response.data.error, "error");
    };
    const data = {};
    data[name] = value;
    return MacroModelService.alterModel(this.client, model.id, data).then(on_success, on_error);
  };

  this.setEnsureUniqueTagFormats = model => {
    return this.setProperty(model, 'ensure_unique_tag_formats', model.ensure_unique_tag_formats);
  };

  this.setEnsureUniqueConfidentFieldName = model => {
    return this.setProperty(model, 'ensure_unique_confident_field_name', model.ensure_unique_confident_field_name);
  };

  this.setPublishingTags = function(model) {
    const on_success = newModel => {
      this.saved = true;
      model.constructor(newModel.original);
      return FlashService.flash("Updated publishing tags for " + model.name, "success");
    };
    const on_error = response => {
      return FlashService.flash(response.data.error, "error");
    };

    return MacroModelService.alterModel(this.client, model.id,
      {tag_formats: JSON.parse(model.tag_formats_string)})
    .then(on_success, on_error);
  };

  this.setConfidentFieldName= function(model) {
    const on_success = newModel => {
      this.saved = true;
      model.constructor(newModel.original);
      return FlashService.flash("Updated Wise Confident Field name for " + model.name, "success");
    };
    const on_error = response => {
      return FlashService.flash(response.data.error, "error");
    };

    return MacroModelService.alterModel(this.client, model.id,
      {confident_field_name: model.confident_field_name})
    .then(on_success, on_error);
  };

  this.setTestEnabled = function(model) {
    return MacroModelService.alterModel(this.client, model.id,
      {test_enabled: model.test_enabled})
    .then(newModel => {
      this.saved = true;
      model.constructor(newModel.original);
      if (model.test_enabled === true) { FlashService.flash("Successfully enabled Runscope test for " + model.name, "success"); }
      if (model.test_enabled === false) { return FlashService.flash("Successfully disabled Runscope test for " + model.name, "success"); }
    });
  };

  this.setTestAlertLevel = function(model) {
    return MacroModelService.alterModel(this.client, model.id,
      {test_alert_level: model.test_alert_level})
    .then(newModel => {
      this.saved = true;
      model.constructor(newModel.original);
      return FlashService.flash("Successfully changed Runscope test alert level for " + model.name, "success");
    });
  };

  this.setTestFrequency = function(model) {
    return MacroModelService.alterModel(this.client, model.id,
      {test_frequency: model.test_frequency})
    .then(newModel => {
      this.saved = true;
      model.constructor(newModel.original);
      return FlashService.flash("Successfully changed Runscope test frequency for " + model.name, "success");
    });
  };

  this.setTestPeriod = function(model) {
    return MacroModelService.alterModel(this.client, model.id,
      {test_period_hours: model.test_period_hours})
    .then(newModel => {
      this.saved = true;
      model.constructor(newModel.original);
      return FlashService.flash("Successfully changed Runscope test period for " + model.name, "success");
    });
  };

  this.setModelConfiguration = function(model) {
    model.model_configuration = null;  // show the spinner
    return MacroModelService.alterModel(this.client, model.id,
      {model_configuration: JSON.parse(model.model_configuration_string)})
    .then(newModel => {
      this.saved = true;
      model.constructor(newModel.original);
      return MacroModelService.fetchModelConfig(this.client, model).then(() => FlashService.flash("Model config updated for " + model.name, "success"));
    });
  };

  this.resetToDefaultModelConfiguration = function(model) {
    return PopupService.popup("Are you sure you want to reset to" +
     " default model config? This will delete all previous custom changes.")
    .then(() => {
      model.model_configuration = null;  // show the spinner
      return MacroModelService.alterModel(this.client, model.id,
        {model_configuration: JSON.parse(null)})
      .then(newModel => {
        model.constructor(newModel.original);
        return MacroModelService.fetchModelConfig(this.client, model).then(() => FlashService.flash("Reset to default model config", "success"));
      });
    });
  };

  this.setCertificationEnabled = function(model) {
    return MacroModelService.alterModel(this.client, model.id,
      {certification_enabled: model.certification_enabled})
    .then(newModel => {
      this.saved = true;
      model.constructor(newModel.original);
      if (model.certification_enabled) {
        return FlashService.flash("Certification is enabled for " + model.name, "success");
      } else {
        return FlashService.flash("Certification is disabled for " + model.name, "success");
      }
    });
  };

  this.updateCertificationSettings = function(model) {
    return MacroModelService.alterModel(this.client, model.id, {
      certification_batch: model.certification_batch,
      certification_period: model.certification_period
    }).then(newModel => {
      this.saved = true;
      model.constructor(newModel.original);
      return FlashService.flash("Successfully updated the Certification settings for " + model.name, "success");
    });
  };

  this.newModel= new MacroModelService.MacroModel({
    name: ""});

  this.saveNewModel = function() {
    const on_success = newModel => {
      this.open = newModel;
      return this.models.push(newModel);
    };
    const on_error = response => {
      return FlashService.flash(response.data.error, "error");
    };

    return MacroModelService.newModel(this.client, this.newModel).then(on_success, on_error);
  };

  this.updateMinOutboundResponsesPerTicket = function(model) {
    return MacroModelService.alterModel(this.client, model.id,
      {min_outbound_responses_per_ticket: model.min_outbound_responses_per_ticket})
    .then(newModel => {
      this.saved = true;
      model.constructor(newModel.original);
      return FlashService.flash("Changed min outbound responses per ticket for " + model.name, "success");
    });
  };

  this.updateAutoResponseSettings = function(model) {
    return MacroModelService.alterModel(this.client, model.id, {
      auto_response_enabled: model.auto_response_enabled,
      auto_response_agent_foreign_id: model.auto_response_agent_foreign_id,
      auto_response_from_address_id: model.auto_response_from_address_id,
      auto_response_max_retries: model.auto_response_max_retries,
      max_auto_responses_per_hour: model.max_auto_responses_per_hour,
      min_time_to_auto_respond_secs: model.min_time_to_auto_respond_secs,
      max_time_to_auto_respond_secs: model.max_time_to_auto_respond_secs,
      auto_response_job_period_secs: model.auto_response_job_period_secs
    }).then(newModel => {
      this.saved = true;
      model.constructor(newModel.original);
      return FlashService.flash("Successfully updated the Auto Response settings for " + model.name, "success");
    });
  };


  this.setup = () => {
    return $q.all([
      MacroModelService.macroModels(this.client),
      AgentService.fetchAgents(this.client.id),
      FromAddressesService.fetchFromAddresses(this.client.id)
    ]).then(values => {
      [this.models, this.agent_responses, this.fromAddresses] = Array.from(values);
      this.agents = this.agent_responses.agents;
      this.loading = false;

      // fetch model configs in background
      return (() => {
        const result = [];
        for (let model of Array.from(this.models)) {
          MacroModelService.fetchModelConfig(this.client, model);
          result.push(model.tag_formats_string = $filter('json')(model.tag_formats));
        }
        return result;
      })();
    });
  };

  this.setup();

  return this;
});
