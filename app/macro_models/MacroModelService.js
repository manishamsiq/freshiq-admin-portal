/*
 * decaffeinate suggestions:
 * DS101: Remove unnecessary use of Array.from
 * DS102: Remove unnecessary code created because of implicit returns
 * DS205: Consider reworking code to avoid use of IIFEs
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.macroModels')
.service('MacroModelService', function($http, config, $filter, MacroService) {

  let MacroModel;
  return {
    MacroModel: (MacroModel = class MacroModel {
      constructor(json) {
        _.extend(this, json);
        this.original = json;
        this.wasEnabled = this.enabled;
        this.original.tag_formats_string = $filter('json')(this.original.tag_formats);
      }

      auto_response_toggleable() {
        return (this.auto_response_agent_foreign_id !== null) && this.has_auto_response_macros;
      }

      resetConfiguration() {
        return this.model_configuration_string = $filter('json')(this.model_configuration);
      }

      unsavedConfiguration() {
        return this.model_configuration_string !== $filter('json')(this.model_configuration);
      }

      resetTags() {
        return this.tag_formats_string = $filter('json')(this.original.tag_formats);
      }

      unsavedTags() {
        return this.tag_formats_string !== $filter('json')(this.original.tag_formats);
      }

      humanize_auto_response_enabled_at() {
        return moment(this.auto_response_enabled_at).fromNow();
      }

      format_auto_response_enabled_at() {
        return moment.utc(this.auto_response_enabled_at).format("YYYY-MM-DD HH:mm:ss UTC");
      }

      humanize_auto_response_last_completed_at() {
        return moment(this.auto_response_last_completed_at).fromNow();
      }

      format_auto_response_last_completed_at() {
        return moment.utc(this.auto_response_last_completed_at).format("YYYY-MM-DD HH:mm:ss UTC");
      }
    }),

    url(client) {
      return config.apiBase + `/api/v1/clients/${client.id}/macro-models`;
    },

    macroModels(client) {
      return $http.get(this.url(client)
      ).then(response => {
        let macro_models = _.map(response.data, macro_model => {
          return new this.MacroModel(macro_model);
        });

        macro_models = _.sortBy(macro_models, 'name');
        return _.sortBy(macro_models, macro_model => !macro_model.enabled);
      });
    },

    model(client, modelId) {
      return $http.get(this.url(client) + "/" + modelId).then(response => {
        return new this.MacroModel(response.data);
      });
    },

    alterModel(client, modelId, data) {
      return $http.patch(this.url(client) + `/${modelId}`,
        {macro_model: data},
        {ignoreLoadingBar: true})
      .then(response => {
        return new this.MacroModel(response.data);
      });
    },

    rebuild(client, model) {
      return $http.post(this.url(client) + `/${model.id}/rebuild`);
    },

    newModel(client, field) {
      return $http.post(this.url(client), {
        macro_model: {
          name: field.name
        }
      }).then(response => {
        return new this.MacroModel(response.data);
      });
    },

    fetchModelConfig(client, model) {
      return $http.get(`${config.apiBase}/api/v1/clients/${client.id}/macro-models/${model.id}/config`).then(function(response) {
        let foreign_id;
        const model_configuration = response.data;
        model.model_configuration = model_configuration;
        model.model_configuration_string = $filter('json')(model_configuration);

        // parse AR macros
        let autoresponse_mask_proportions = {};
        if (model_configuration && model_configuration["post-predict"] && model_configuration["post-predict"]["autoresponse_mask_proportion"]) {
          autoresponse_mask_proportions = model_configuration["post-predict"]["autoresponse_mask_proportion"];
        }

        model.auto_response_macros = {};
        if (model_configuration && model_configuration.predict && model_configuration.predict.confidence) {
          for (let confidence of Array.from(model_configuration.predict.confidence)) {
            if (confidence.confidence_name === "autoresponse") {
              for (foreign_id in confidence.confidence_threshold) {
                const threshold = confidence.confidence_threshold[foreign_id];
                model.auto_response_macros[foreign_id] = {
                  foreign_id,
                  confidence_threshold: threshold,
                  mask_proportion: autoresponse_mask_proportions[foreign_id]};
              }
            }
          }
        }

        model.has_auto_response_macros = Object.keys(model.auto_response_macros).length > 0;

        // fetch AR macro titles in background
        if (client.data_source.name === 'zendesk') {
          return (() => {
            const result = [];
            for (foreign_id in model.auto_response_macros) {
              const _ = model.auto_response_macros[foreign_id];
              result.push(MacroService.fetchMacro(client.id, foreign_id).then(function(macro) {
                const title = macro && macro.title ? macro.title : 'Macro not found in support DB';
                return model.auto_response_macros[macro.foreign_id].title = title;
              }));
            }
            return result;
          })();
        } else {
          return (() => {
            const result1 = [];
            for (foreign_id in model.auto_response_macros) {
              const _ = model.auto_response_macros[foreign_id];
              result1.push(MacroService.fetchEmailTemplate(client.id, foreign_id).then(function(emailTemplate) {
                const title = emailTemplate ? emailTemplate.title : 'Email Template not found in support DB';
                return model.auto_response_macros[emailTemplate.foreign_id].title = title;
              }));
            }
            return result1;
          })();
        }
      });
    }
  };
});
