/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.signup')
.controller('ConnectToServicecloudCtrl',
function(SignupService, FlashService, $state, $stateParams) {

  this.formSubmit = () => {
    this.loading = true;
    const redirectTo = $state.href('signup.new-user');
    const failureTo = $state.href('signup.connect-to-servicecloud',
      {errorMessage: "Error connecting to Salesforce"});

    return location.href = SignupService.servicecloudOauthURL(redirectTo, failureTo,
      this.sandbox);
  };

  return this;
});
