/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.signup')
.service('SignupService', function($http, config, $location) {

  return {
    url: `${config.apiBase}/api/v1`,

    newUser(given_name, surname, email, password, phone) {
      return $http.post(this.url+"/users", {
          given_name,
          surname,
          email,
          password,
          phone
        }
      );
    },

    zendeskOauthURL(subdomain, redirect, error) {
      const base = $location.protocol() + "://" + $location.host() +
        ":" + $location.port().toString();

      const redirectURL = encodeURIComponent(base + redirect);
      const errorURL = encodeURIComponent(base + error);
      return config.apiBase +
        `/oauth/zendesk?subdomain=${subdomain}` +
        `&redirect_url=${redirectURL}&error_url=${errorURL}`;
    },

    servicecloudOauthURL(redirect, error, sandbox) {
      const base = $location.protocol() + "://" + $location.host() +
        ":" + $location.port().toString();

      const redirectURL = encodeURIComponent(base + redirect);
      const errorURL = encodeURIComponent(base + error);

      let url = config.apiBase +
        "/oauth/servicecloud?" +
        `&redirect_url=${redirectURL}` +
        `&error_url=${errorURL}`;

      if (sandbox) {
        url += `&sandbox=${sandbox}`;
      }

      return url;
    },


    connectWithAPIKey(foreign_id, email, apiToken) {
      return $http.post(this.url+"/clients", {
        foreign_id,
        api_email: email,
        api_token: apiToken
      }
      );
    }
  };
});
