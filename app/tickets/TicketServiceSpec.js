/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

describe("TicketFieldService:", function() {
  beforeEach(module("wiseSupport.tickets"));

  beforeEach(inject(function(TicketService,
                     $httpBackend,
                     config,
                     $rootScope,
                     $q) {
    this.TicketService = TicketService;
    this.$httpBackend = $httpBackend;
    this.config = config;
    this.$q = $q;
    this.scope = $rootScope.$new();
    this.ticketResponse = {
      count: 103,
      offset: 0,
      tickets: [
        {
          id: 3
        },
        {
          id: 4
        },
        {
          id: 5
        },
        {
          id: 6
        },
      ]
    };


    this.clientId = 42;
    return this.url = `${this.config.apiBase}/api/v1/clients/${this.clientId}/\
tickets?limit=4&offset=0`;
  })
  );

  describe("url()", () => it("computes the URL from the clientId on ClientService", function() {
    const expected = `${this.config.apiBase}/api/v1/clients/${this.clientId}/tickets`;
    return expect(this.TicketService.url(this.clientId)).toBe(expected);
  }));

  return describe("tickets()", function() {
    beforeEach(function() {
      this.$httpBackend.whenGET(this.url).respond(this.ticketResponse);
      return this.resultPromise = this.TicketService.tickets(this.clientId, {
        page: 0,
        perPage: 4
      }
      );
    });

    afterEach(function() {
      return this.$httpBackend.flush();
    });

    it("gets the count", function() {
      return this.resultPromise.then(result => expect(result.count).toBe(103));
    });

    it("gets the offset", function() {
      return this.resultPromise.then(result => expect(result.offset).toBe(0));
    });

    return it("populates the list with ticket objects", function() {
      const tickets = _.map(this.ticketResponse.tickets, ticket => {
        return new this.TicketService.Ticket(ticket);
      });

      return this.resultPromise.then(result => // Use toString here to handle deep equality.
      expect(result.tickets.toString()).toEqual(tickets.toString()));
    });
  });
});
