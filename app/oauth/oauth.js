/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.oauth', [
  'ui.router'
]);

angular.module('wiseSupport.oauth').config($stateProvider => $stateProvider
  .state('oauth', {
    url: '/oauth/step2?state&code',
    template: "",

    resolve: {
      princessInAnotherCastle($stateParams, config) {
        return window.location = `${config.apiBase}/oauth/zendesk/step2` +
                          `?state=${$stateParams.state}` +
                          `&code=${$stateParams.code}`;
      }
    }
  }
));
