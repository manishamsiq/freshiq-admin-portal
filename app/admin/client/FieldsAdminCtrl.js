/*
 * decaffeinate suggestions:
 * DS101: Remove unnecessary use of Array.from
 * DS102: Remove unnecessary code created because of implicit returns
 * DS205: Consider reworking code to avoid use of IIFEs
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.admin')
.controller('FieldsAdminCtrl',
function(client, TicketFieldService, FlashService, PopupService, $timeout) {

  this.client = client;
  this.open = null;
  this.loading = true;
  this.fields = null;
  this.fetching = false;
  this.fetched_at = null;

  this.fetch = () => {
    this.fetching = true;
    return TicketFieldService.fetch(this.client.id).then(() => {
      this.fetching = false;
      return this.setup();
    });
  };

  this.rebuildField = function(field) {
    return TicketFieldService.rebuildField(this.client.id, field.id).then(() => FlashService.flash("Rebuilding field: " + field.name, "success"));
  };

  this.deleteValues = function(field) {
    const field_name = field.name;
    return PopupService.popup("Are you sure you want to delete all ticket field values for " + field.name + "?")
    .then(() => {
      const on_success = newField => {
        this.saved = true;
        return FlashService.flash("Deleting ticket field values for " + field_name + ".", "success");
      };
      const on_error = response => {
        return FlashService.flash(response.data.error, "error");
      };
      return TicketFieldService.deleteValues(this.client.id, field.id).then(on_success, on_error);
    });
  };

  this.setModelConfiguration = function(field) {
    field.model_configuration = null;  // show the spinner
    return TicketFieldService.alterField(this.client.id, field.id,
      {model_configuration: JSON.parse(field.model_configuration_string)})
    .then(newField => {
      this.saved = true;
      field.constructor(newField.original);
      return TicketFieldService.fetchModelConfig(this.client.id, field);
    });
  };

  this.resetToDefaultModelConfiguration = function(field) {
    return PopupService.popup("Are you sure you want to reset to" +
     " default model config? This will delete all previous custom changes.")
    .then(() => {
      field.model_configuration = null;  // show the spinner
      return TicketFieldService.alterField(this.client.id, field.id,
        {model_configuration: JSON.parse(null)})
      .then(newField => {
        field.constructor(newField.original);
        return TicketFieldService.fetchModelConfig(this.client.id, field);
      });
    });
  };

  this.saveSchedule = function(field) {
    return TicketFieldService.alterField(this.client.id, field.id,
      {rebuild_schedule: field.rebuild_schedule})
    .then(newField => {
      this.saved = true;
      return field.constructor(newField.original);
    });
  };

  this.setStore = function(field) {
    return TicketFieldService.alterField(this.client.id, field.id,
      {store: field.store})
    .then(newField => {
      this.saved = true;
      return field.constructor(newField.original);
    });
  };

  this.setEnabled = function(field) {
    return TicketFieldService.alterField(this.client.id, field.id,
      {enabled: field.enabled})
    .then(newField => {
      this.saved = true;
      field.constructor(newField.original);
      return TicketFieldService.fetchModelConfig(this.client.id, field);
    });
  };

  this.setTestEnabled = function(field) {
    return TicketFieldService.alterField(this.client.id, field.id,
      {test_enabled: field.test_enabled})
    .then(newField => {
      this.saved = true;
      field.constructor(newField.original);
      if (field.test_enabled === true) { FlashService.flash("Successfully enabled Runscope test", "success"); }
      if (field.test_enabled === false) { return FlashService.flash("Successfully disabled Runscope test", "success"); }
    });
  };

  this.setTestAlertLevel = function(field) {
    return TicketFieldService.alterField(this.client.id, field.id,
      {test_alert_level: field.test_alert_level})
    .then(newField => {
      this.saved = true;
      field.constructor(newField.original);
      return FlashService.flash("Successfully changed Runscope test alert level", "success");
    });
  };

  this.setTestFrequency = function(field) {
    return TicketFieldService.alterField(this.client.id, field.id,
      {test_frequency: field.test_frequency})
    .then(newField => {
      this.saved = true;
      field.constructor(newField.original);
      return FlashService.flash("Successfully changed Runscope test frequency", "success");
    });
  };

  this.setTestPeriod = function(field) {
    return TicketFieldService.alterField(this.client.id, field.id,
      {test_period_hours: field.test_period_hours})
    .then(newField => {
      this.saved = true;
      field.constructor(newField.original);
      return FlashService.flash("Successfully changed Runscope test period", "success");
    });
  };

  this.setPublished = function(field) {
    return TicketFieldService.alterField(this.client.id, field.id,
      {publish_predictions: field.publish_predictions})
    .then(newField => {
      this.saved = true;
      return field.constructor(newField.original);
    });
  };

  this.setPublishToField = function(field) {
    return TicketFieldService.alterField(this.client.id, field.id,
      {publish_to_field: field.publish_to_field})
    .then(newField => {
      this.saved = true;
      return field.constructor(newField.original);
    });
  };

  this.setCertificationEnabled = function(field) {
    return TicketFieldService.alterField(this.client.id, field.id,
      {certification_enabled: field.certification_enabled})
    .then(newField => {
      this.saved = true;
      field.constructor(newField.original);
      if (field.certification_enabled) {
        return FlashService.flash("Certification is enabled for " + field.name, "success");
      } else {
        return FlashService.flash("Certification is disabled for " + field.name, "success");
      }
    });
  };

  this.updateCertificationSettings = function(field) {
    return TicketFieldService.alterField(this.client.id, field.id, {
      certification_batch: field.certification_batch,
      certification_period: field.certification_period
    }).then(newField => {
      this.saved = true;
      field.constructor(newField.original);
      return FlashService.flash("Successfully updated the Certification settings", "success");
    });
  };

  this.setNullValues = function(field) {
    return TicketFieldService.alterField(this.client.id, field.id,
      {factory_uses_null_values: field.factory_uses_null_values})
    .then(newField => {
      this.saved = true;
      return field.constructor(newField.original);
    });
  };

  this.newField = new TicketFieldService.TicketField({
    name: "",
    zendesk_id: null,
    options: [{ name: "", value: "" }]});

  this.saveNewField = function() {
    return TicketFieldService.newField(this.client.id, this.newField).then(field => {
      this.open = field;
      return this.fields.push(field);
    });
  };

  this.writeField = function(field) {
    return TicketFieldService.writeField(this.client.id, field).then(newField => {
      this.saved = true;
      return field.constructor(newField.original);
    });
  };

  this.deleteField = function(field) {
    if (window.confirm(`Delete field ${field.name}?`)) {
      return TicketFieldService.deleteField(this.client.id, field).then(() => {
        this.fields = _.without(this.fields, field);
        return this.open = null;
      });
    }
  };

  this.hasFetched = () => {
    return this.fetched_at !== null;
  };

  this.humanizeFetchedAt = () => {
    return moment(this.fetched_at).fromNow();
  };

  this.formatFetchedAt = () => {
    return moment.utc(this.fetched_at).format("YYYY-MM-DD HH:mm:ss UTC");
  };

  this.setup = () => {
    this.open = null;
    this.loading = true;
    this.fields = null;
    this.fetching = false;
    this.fetched_at = null;

    return TicketFieldService.ticketFields(this.client.id, false).then(data => {
      this.fetched_at = data.fetched_at;
      this.fields = data.ticket_fields;
      this.loading = false;

      const delay = (ms, f) => $timeout(f, ms);
      // fetch model configs in the background (enabled first)
      // delay a bit for faster initial loading
      delay(1000, () => {
        return (() => {
          const result = [];
          for (let field of Array.from(this.fields)) {
            if (field.enabled) {
              result.push(TicketFieldService.fetchModelConfig(this.client.id, field));
            } else {
              result.push(undefined);
            }
          }
          return result;
        })();
      });

      // ... then fetch configs for disabled fields.
      // delay a bit for faster initial loading
      return delay(2000, () => {
        return (() => {
          const result = [];
          for (let field of Array.from(this.fields)) {
            if (!field.enabled) {
              result.push(TicketFieldService.fetchModelConfig(this.client.id, field));
            } else {
              result.push(undefined);
            }
          }
          return result;
        })();
      });
    });
  };

  this.setup();

  return this;
});
