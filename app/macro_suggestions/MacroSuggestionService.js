/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.macroSuggestions')
.service('MacroSuggestionService', ($http, config) => ({
  update(clientId, id, params) {
    return $http({
      method: 'PUT',
      url: `${config.apiBase}/api/v1/clients/${clientId}/macro-suggestions/${id}`,
      data: params
    });
  },

  index(clientId) {
    return $http.get(`${config.apiBase}/api/v1/clients/${clientId}/macro-suggestions`);
  }
}));
