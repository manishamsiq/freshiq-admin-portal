/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.user')
.service('UserService', function($http, config) {

  let User;
  return {
    User: (User = class User {
      constructor(json) {
        _.extend(this, json);
        this.fullName = this.given_name + " " + this.surname;
      }

      isAdmin() {
        return _.includes(this.roles, "admin");
      }

      hasAdminPrivilege() {
        return _.includes(this.email, "@answeriq.com");
      }
    
      setupMixpanel() {
        if (window.mixpanel) {
          mixpanel.identify(this.email);
          return mixpanel.people.set({
            "$email": this.email,
            "$name": this.fullName,
            "Client": this.subdomain
          });
        }
      }
    }),


    authUrl: `${config.apiBase}/api/v1/auth`,

    me() {
      // Allowing this to return `undefined` means me *always* resolves;
      // that allows us to state-resolve-redirect to login when necessary.
      // Also, if the server doesn't respond in 3 seconds, just give up.
      return $http.get(this.authUrl + "/me", {timeout: 3000})
      .then(response => {
        const user = new this.User(response.data);
        user.setupMixpanel();
        return user;
      }
      , () => undefined);
    },

    halfSignedUpMe() {
      return $http.get(this.authUrl + "/foreign-api-me").then(function(response) {
        const me = response.data;
        const names = me.name.split(" ");
        me.surname = names.pop();
        me.givenName = names.join(" ");
        return me;
      });
    },

    login(email, password) {
      return $http.post(this.authUrl, { email, password })
      .then(response => {
        return new this.User(response.data);
      });
    },


    logout() {
      return $http.delete(this.authUrl);
    },

    sendPasswordResetEmail(email) {
      const url = this.authUrl + "/reset-password";
      return $http.post(url, {email});
    },

    resetPassword(token, password) {
      const url = this.authUrl + "/reset-password";
      return $http.put(url, {token, password});
    },

    url(client_id) {
      return `${config.apiBase}/api/v1/clients/${client_id}/users`;
    },

    usersForClient(clientId) {
      return $http.get(this.url(clientId)).then(response => {
        return _.map(response.data.users, user => {
          return new this.User(user);
        });
      });
    },

    update(clientId, user, properties) {
      const url = this.url(clientId) + "/by_user_href";
      properties.user_href = user.href;
      return $http.post(url, properties).then(response => {
        return new this.User(response.data);
      });
    },

    delete(clientId, user) {
      const url = this.url(clientId) + "/by_user_href";
      return $http.delete(url, {data: { user_href: user.href }});
    }
  };
});
