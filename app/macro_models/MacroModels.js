// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.macroModels', [
  'ui.router',
  'ngDialog',
  'wiseSupport.client',
  'wiseSupport.config',
  'wiseSupport.flash'
]);
