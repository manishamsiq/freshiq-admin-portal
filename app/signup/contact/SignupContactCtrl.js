/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.signup')
.controller('SignupContactCtrl', $stateParams => hbspt.forms.create({
  portalId: '307358',
  formId: $stateParams.formGUID,
  target: "#hubspot-form",
  css: "",
  submitButtonClass: "primary button",
  requiredCss: "validation-error"
}));
