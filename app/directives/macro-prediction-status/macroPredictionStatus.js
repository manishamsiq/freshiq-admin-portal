/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS103: Rewrite code to no longer use __guard__
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.macroPredictionStatus', [])
  .directive('macroPredictionStatus', () => ({
  restrict: 'E',

  scope: {
    recommendation: '=',
    showHeader: "@"
  },

  templateUrl: "/directives/macro-prediction-status" +
    "/macro-prediction-status.html",

  link(scope, element) {
    element.addClass("tooltip-item");
    if (!(scope.recommendation != null ? scope.recommendation.prediction_probabilities : undefined)) {
      return element.addClass("no-prediction");
    }
  },

  controller($scope, MacroService) {
    const observedId = $scope.recommendation != null ? $scope.recommendation.observation_macro_zendesk_id : undefined;
    $scope.observedMacro = MacroService.getMacroByZendeskId(observedId);

    if ($scope.recommendation != null ? $scope.recommendation.prediction_probabilities : undefined) {
      const predictedId =
        __guard__($scope.recommendation != null ? $scope.recommendation.prediction_probabilities[0] : undefined, x => x.zendesk_id);
      return $scope.predictedMacro = MacroService.getMacroByZendeskId(predictedId);
    }
  }
}));

function __guard__(value, transform) {
  return (typeof value !== 'undefined' && value !== null) ? transform(value) : undefined;
}