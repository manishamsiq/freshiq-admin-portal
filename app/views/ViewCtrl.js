/*
 * decaffeinate suggestions:
 * DS101: Remove unnecessary use of Array.from
 * DS102: Remove unnecessary code created because of implicit returns
 * DS201: Simplify complex destructure assignments
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.views')
.controller('ViewCtrl', function(client, ViewService, FlashService, $stateParams, $q) {
  this.client = client;
  this.views = null;
  this.view_templates = null;
  this.loading = true;
  this.fetching = false;
  this.form = {
    "show": false,
    "error": false,
    "saving": false,
  };
  this.confirm_delete = {
    "show": false,
    "error": false,
    "deleting": false
  };

  this.isSupported = () => {
    return this.client.data_source.name === "zendesk";
  };

  this.hasFetched = () => {
    return this.lastFetchedAt() !== undefined;
  };

  this.lastFetchedAt = () => {
    const array = _.sortBy(this.views, v => v.fetched_at), last = array[array.length - 1];
    return (last != null ? last.fetched_at : undefined);
  };

  this.humanizeLastFetchedAt = () => {
    return moment(this.lastFetchedAt()).fromNow();
  };

  this.formatLastFetchedAt = () => {
    return moment.utc(this.lastFetchedAt()).format("YYYY-MM-DD HH:mm:ss UTC");
  };

  this.prettyJSON = json => {
    return JSON.stringify(json, undefined, 4);
  };

  this.externalLink = view => {
    return `https://${this.client.name}.zendesk.com/agent/filters/${view.foreign_id}`;
  };

  this.closeForm = () => {
    this.form.error = false;
    this.form.show = false;
    return this.form.saving = false;
  };

  this.closeConfirmDelete = () => {
    this.confirm_delete.error = false;
    this.confirm_delete.show = false;
    return this.confirm_delete.deleting = false;
  };

  this.fetchNow = () => {
    this.fetching = true;
    const success = response => {
      this.views = response.data;
      return this.fetching = false;
    };
    const error = response => {
      FlashService.flash(`Error while fetching views: ${this.prettyJSON(response.data)}`, 'error');
      return this.fetching = false;
    };
    return ViewService.fetchNow(this.client.id).then(success, error);
  };

  this.new = () => {
    return this.form = {
      "show": true,
      "view": {
        "foreign_attributes": {
          "title": "",
          "conditions": {
            "all": [],
            "any": []
          }
        }
      },
      "title": "New View",
      "button_text": "Create",
    };
  };

  this.newViewFromTemplateId = template_id => {
    const view_template = _.find(this.view_templates, vt => vt.id === template_id);
    return this.form = {
      "show": true,
      "view": {
        "foreign_attributes": view_template.foreign_attributes
      },
      "description": view_template.description,
      "title": `New View from \"${view_template.foreign_attributes.title}\"`,
      "button_text": "Create",
    };
  };

  this.edit = id => {
    const view = _.find(this.views, v => v.id === id);
    return this.form = {
      "show": true,
      "view": view,
      "title": `Edit View: ${view.foreign_attributes.title}`,
      "button_text": "Update"
    };
  };

  this.confirmDelete = id => {
    const view = _.find(this.views, v => v.id === id);
    return this.confirm_delete = {
      "show": true,
      "view": view
    };
  };

  this.save = () => {
    let foreign_attributes;
    this.form.saving = true;
    this.form.error = false;
    const {
      view
    } = this.form;
    const save_type = (view.id === undefined) ? "create" : "update";

    try {
      foreign_attributes = JSON.parse(angular.element(document.querySelector('#form-view-foreign-attributes')).val());
    } catch (e) {
      this.form.error = e.toString();
      this.form.saving = false;
      return;
    }

    const success = response => {
      const saved_view = response.data;
      FlashService.flash(`Successfully ${save_type}d View: ${saved_view.foreign_attributes.title}`, 'success');
      this.fetchNow();
      return this.closeForm();
    };
    const error = response => {
      this.form.error = this.prettyJSON(response.data);
      return this.form.saving = false;
    };

    if (save_type === "update") {
      return ViewService.update(this.client.id, view.id, foreign_attributes).then(success, error);
    } else {
      return ViewService.create(this.client.id, foreign_attributes).then(success, error);
    }
  };

  this.delete = () => {
    this.confirm_delete.deleting = true;
    this.confirm_delete.error = false;
    const {
      view
    } = this.confirm_delete;

    const success = response => {
      FlashService.flash(`Successfully deleted View: ${view.foreign_attributes.title}`, 'success');
      this.fetchNow();
      return this.closeConfirmDelete();
    };
    const error = response => {
      this.confirm_delete.error = this.prettyJSON(response.data);
      return this.confirm_delete.deleting = false;
    };
    return ViewService.delete(this.client.id, view.id).then(success, error);
  };

  this.setup = () => {
    this.loading = true;
    return $q.all([
      ViewService.views($stateParams.clientId),
      ViewService.viewTemplates($stateParams.clientId)
    ]).then(values => {
      [this.views, this.view_templates] = Array.from(values);
      return this.loading = false;
    });
  };

  this.setup();

  return this;
});
