/*
 * decaffeinate suggestions:
 * DS101: Remove unnecessary use of Array.from
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.admin')
.controller('UnifiedWidgetCtrl',
function(client, AgentService, UnifiedWidgetConfigService, FlashService, $document, $q, $filter) {
  // state
  this.client = client;
  this.is_loading = true;
  this.default_config = null;
  this.client_config = null;
  this.agents = null;
  this.modal = {};
  this.modal_version = null;
  this.modal_version_type = null;
  this.modal_diff_html = null;

  this.search_string = "";
  this.result_search_string = "";
  this.total_count = null;
  this.per_page = 20;
  this.page = 1;
  this.page_range_start = null;
  this.page_range_end = null;
  this.show_next = false;
  this.show_previous = false;

  this.fetchConfigsAndAgents = () => {
    let page, per_page, search_string, with_tokens_only;
    this.is_loading = true;
    this.agents = null;

    return $q.all([
      AgentService.fetchAgents(
        this.client.id,
        (with_tokens_only=""),
        (search_string=this.search_string),
        (page=this.page),
        (per_page=this.per_page)
      ),
      UnifiedWidgetConfigService.all(this.client.id)
    ]).then(values => {
      const [agent_response, configs] = Array.from(values);

      this.total_count = agent_response.total_count;
      const {
        page_count
      } = agent_response;
      this.show_next = page_count > this.page;
      this.show_previous = this.page > 1;
      this.result_search_string = this.search_string;

      this.page_range_start = ((this.page-1) * this.per_page) + 1;
      this.page_range_end = Math.min(this.page * this.per_page, this.total_count);

      _.each(configs, function(config) {
        config.pretty_config = $filter('json')(config.current_config);
        return _.each(config.versions, version => version.pretty_config = $filter('json')(version.config));
      });

      this.default_config = _.find(configs, c => {
        return !c.client_id;
      });

      this.client_config = _.find(configs, c => {
        return (c.client_id === this.client.id) && !c.agent_foreign_id;
      });

      this.agents = _.map(agent_response.agents, agent => {
        agent.config = _.find(configs, c => {
          return (c.client_id === this.client.id) && (c.agent_foreign_id === agent.foreign_id);
        });
        return agent;
      });

      return this.is_loading = false;
    });
  };

  this.goToNextPage = () => {
    this.page = this.page + 1;
    return this.fetchConfigsAndAgents();
  };

  this.goToPreviousPage = () => {
    if (this.page > 1) {
      this.page = this.page - 1;
      return this.fetchConfigsAndAgents();
    }
  };

  this.search = () => {
    this.page = 1;
    return this.fetchConfigsAndAgents();
  };

  this.clearSearch = () => {
    this.search_string = "";
    return this.search();
  };

  this.humanizeDateString = date_string => moment(date_string).fromNow();

  this.modalVersions = () => {
    if (!this.modal.client_id) {
      return this.default_config.versions;
    } else if (!this.modal.agent_foreign_id) {
      return this.client_config.versions;
    } else {
      const agent = _.find(this.agents, a => a.foreign_id === this.modal.agent_foreign_id);
      return agent.config.versions;
    }
  };

  this.showHistory = () => {
    this.modal_version = null;
    return this.modal_version_type = null;
  };

  this.showVersion = version => {
    this.modal_version = version;
    return this.modal_version_type = "show";
  };

  this.nextShowVersion = () => {
    const next_version = _.find(this.modalVersions(), {version: this.modal_version.version+1});
    if (next_version) {
      return this.showVersion(next_version);
    }
  };

  this.previousShowVersion = () => {
    const previous_version = _.find(this.modalVersions(), {version: this.modal_version.version-1});
    if (previous_version) {
      return this.showVersion(previous_version);
    }
  };

  this.diffVersion = version => {
    this.modal_version = version;
    this.modal_version_type = "diff";

    try {
      const previous_version = _.find(this.modalVersions(), {version: version.version-1});
      const original_json_config = previous_version ? previous_version.config : {};
      const new_json_config = version.config;
      const delta = jsondiffpatch.create({
        objectHash(obj) { return obj.id; }
        ,
        arrays: {
            detectMove: true,
            includeValueOnMove: true
        },
      }).diff(original_json_config, new_json_config);

      if (delta) {
        return this.modal_diff_html = jsondiffpatch.formatters.html.format(delta, new_json_config);
      } else {
        return this.modal_diff_html = "No changes";
      }

    } catch (error) {
      return this.modal_diff_html = error;
    }
  };

  this.nextDiffVersion = () => {
    const next_version = _.find(this.modalVersions(), {version: this.modal_version.version+1});
    if (next_version) {
      return this.diffVersion(next_version);
    }
  };

  this.previousDiffVersion = () => {
    const previous_version = _.find(this.modalVersions(), {version: this.modal_version.version-1});
    if (previous_version) {
      return this.diffVersion(previous_version);
    }
  };

  this.showDefaultConfig = () => {
    this.modal = {
      type: "showDefaultConfig",
      client_id: null,
      agent_foreign_id: null,
      title: "Edit Default Config",
      config: this.default_config.pretty_config,
      versions: this.default_config.versions,
      button_title: "Update",
      show_delete: false
    };
    return this.showModal();
  };

  this.showClientConfig = () => {
    if (this.client_config.current_config) {
      this.modal = {
        type: "showClientConfig",
        client_id: this.client.id,
        agent_foreign_id: null,
        title: `Edit Client Config (${this.client.name})`,
        config: this.client_config.pretty_config,
        versions: this.client_config.versions,
        button_title: "Update",
        show_delete: true
      };
    } else {
      const parent_version = this.default_config.versions[0].version;
      this.modal = {
        type: "showClientConfig",
        client_id: this.client.id,
        agent_foreign_id: null,
        title: `New Client Config (${this.client.name})`,
        description: `Created from the Default Config (version #${parent_version})`,
        config: this.default_config.pretty_config,
        versions: this.client_config.versions,
        button_title: "Create",
        show_delete: false,
      };
    }
    return this.showModal();
  };

  this.showAgentConfig = agent => {
    if (agent.config.current_config) {
      this.modal = {
        type: "showAgentConfig",
        client_id: this.client.id,
        agent_foreign_id: agent.foreign_id,
        title: `Edit config for \"${agent.name}\"`,
        button_title: "Update",
        config: agent.config.pretty_config,
        versions: agent.config.versions,
        show_delete: true
      };
    } else {
      const parent = this.client_config.current_config ? this.client_config : this.default_config;
      const parent_name = this.client_config.current_config ? "Client Config" : "Default Config";
      const parent_version = parent.versions[0].version;
      this.modal = {
        type: "showAgentConfig",
        client_id: this.client.id,
        agent_foreign_id: agent.foreign_id,
        title: `New config for \"${agent.name}\"`,
        button_title: "Create",
        description: `Created from the ${parent_name} (version #${parent_version})`,
        config: parent.pretty_config,
        versions: agent.config.versions,
        show_delete: false
      };
    }
    return this.showModal();
  };

  this.showModal = () => {
    const modal = document.querySelector('.config-modal');
    const content_container = modal.querySelector('.content-container');
    const content = modal.querySelector('.content');
    const background = modal.querySelector('.background');

    modal.style.display = 'block';
    content_container.style.top = window.scrollY + 50 + "px";

    // close when clicking OUTSIDE modal
    background.addEventListener('click', this.closeModal, false);
    content_container.addEventListener('click', this.closeModal, false);

    // don't close when clicking INSIDE modal
    const stopPropagation = e => e.stopPropagation();
    return content.addEventListener('click', stopPropagation, false);
  };

  this.reloadModal = () => {
    if (this.modal.type === "showAgentConfig") {
      const agent = _.find(this.agents, a => a.foreign_id === this.modal.agent_foreign_id);
      return this.showAgentConfig(agent);
    } else if (this.modal.type === "showClientConfig") {
      return this.showClientConfig();
    } else if (this.modal.type === "showDefaultConfig") {
      return this.showDefaultConfig();
    }
  };

  this.closeModal = () => {
    const modal = document.querySelector(".config-modal");
    modal.style.display = 'none';
    this.modal_version = null;
    return this.modal_version_type = null;
  };

  this.validate = () => {
    const errors = [];

    // description
    if (this.modal.description === "") {
      errors.push("Please give a description");
    }

    // json
    try {
      JSON.parse(this.modal.config);
    } catch (error) {
      errors.push(error.toLocaleString());
    }

    this.modal.valid = errors.length === 0;
    return this.modal.validation_error = errors.join(', ');
  };


  this.saveModal = () => {
    this.validate();
    if (this.modal.valid) {
      this.modal.saving = true;
      return UnifiedWidgetConfigService.put(
        this.modal.client_id,
        this.modal.agent_foreign_id,
        JSON.parse(this.modal.config),
        this.modal.description)
      .then(() => {
        return this.fetchConfigsAndAgents().then(() => {
          this.modal.saving = false;
          return this.reloadModal();
        });
      });
    }
  };

  this.deleteConfig = () => {
    if (window.confirm("Are you sure?")) {

      // validate description
      if (this.modal.description === "") {
        this.modal.validation_error =  "Please give a description";
        this.modal.valid = false;
      } else {
        this.modal.valid = true;
      }

      if (this.modal.valid) {
        this.modal.saving = true;
        return UnifiedWidgetConfigService.delete(this.client.id, this.modal.agent_foreign_id, this.modal.description).then(() => {
          return this.fetchConfigsAndAgents().then(() => {
            this.modal.saving = false;
            return this.closeModal();
          });
        });
      }
    }
  };

  // close modal with escape key
  $document.on("keyup", event => {
    if (event.keyCode === 27) {
      return this.closeModal();
    }
  });

  // load agents and configs in background
  this.fetchConfigsAndAgents();

  return this;
});
