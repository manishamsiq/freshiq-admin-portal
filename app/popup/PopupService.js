/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.popup')
.service('PopupService', function(ngDialog) {

  return {
    message: "",

    popup(message) {
      this.message = message;
      return ngDialog.openConfirm({template: '/popup/popup.html'},
        {className: 'ngdialog-theme-default'});
    }
  };
});
