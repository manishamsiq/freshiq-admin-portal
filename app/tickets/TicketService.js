/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS103: Rewrite code to no longer use __guard__
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.tickets')
.service('TicketService', function($http, config) {

  let Ticket;
  return {
    Ticket: (Ticket = class Ticket {
      constructor(json) {
        _.extend(this, json);
        this.date = Date.parse(this.zendesk_created_at);
        this.values = this.value_dict;
      }

      observation(field) {
        if (field) { return this.values[field.id].observation; }
      }

      prediction(field) {
        if (field) { return this.values[field.id].prediction; }
      }

      isOpen() {
        return _.contains(["open", "new"], this.zendesk_status);
      }

      isCorrect(field) {
        if (field) {
          return !this.isOpen() && (__guard__(this.prediction(field), x => x.id) === __guard__(this.observation(field), x1 => x1.id));
        }
      }

      isIncorrect(field) {
        if (field) {
          return !this.isOpen() && !(__guard__(this.prediction(field), x => x.id) === __guard__(this.observation(field), x1 => x1.id));
        }
      }

      isConfident(field) {
        if (field) {
          return this.values[field.id].confident;
        }
      }

      preableDescription() {
        // This is a description that's been stripped of silly things
        // like long URLs and mid-sentence line breaks.

        let description = this.description.replace(
          new RegExp(`\\S*(mailto\\:|news|www\\.|(ht|f)tp(s?)\\://)\\S+`, 'g'),
          "[removed link]"
        );
        description = description.replace(new RegExp(`\\r?\\n(?![\\r\\n>])`, 'g'), " ");
        return description.replace(new RegExp(`(\\r?\\n( *)(?!>))+`, 'g'), "\n\n");
      }
    }),

    url(clientId) {
      return config.apiBase + `/api/v1/clients/${clientId}/tickets`;
    },

    tickets(clientId,
      {
        page,
        perPage,
        ticketField,
        predictionFilter,
        search,
        sortBy,
        sortDirection,
        displayResponseRec
      }) {

      return $http.get(this.url(clientId), { params: {
        limit: perPage,
        offset: page * perPage,
        ticket_field: ticketField,
        prediction_filter: predictionFilter,
        search,
        sort_by: sortBy,
        sort_direction: sortDirection,
        response_recommendations: displayResponseRec
      }
    }
      ).then(response => {
        const {
          data
        } = response;
        data.tickets = _.map(response.data.tickets, ticket => {
          return new this.Ticket(ticket);
        });
        data.offset = parseInt(data.offset);
        return data;
      });
    }
  };
});

function __guard__(value, transform) {
  return (typeof value !== 'undefined' && value !== null) ? transform(value) : undefined;
}