/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.admin')
.controller('ClientListCtrl', function(ClientService, TicketFieldService) {

  this.list_type = 'active';
  this.clients = null;
  this.loading = true;

  this.update = () => {
    this.loading = true;
    const active_only = (this.list_type === 'active');
    const inactive_only = (this.list_type === 'inactive');
    return ClientService.get_all(active_only, inactive_only).then(clients => {
      this.clients = clients;
      return this.loading = false;
    });
  };

  this.rebuild_all = client => TicketFieldService.rebuildFields(client).then(() => FlashService.flash(
    "Starting new builds for all fields in " + client.name, "success"));

  this.update();

  return this;
});
