/*
 * decaffeinate suggestions:
 * DS101: Remove unnecessary use of Array.from
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.macroSuggestions')
.controller('MacroSuggestionsCtrl',
function($document, $sce, MacroSuggestionService, FlashService, client, route) {
  this.client = client;
  this.route = route;

  this.text_preview_length = 200;

  this.common_replies = [];
  this.multiple_macros = [];
  this.generated_at = null;

  this.macro_suggestion_table_type = 'common-replies';
  this.fetching = true;

  this.filters = {
    "suggested": false,
    "saved": false,
    "removed": true,
    "created": true
  };

  // sort state
  this.sort_column = "num_tickets";
  this.sort_desc = true;

  // fetch macro suggestions on load
  let on_success = response => {
    this.common_replies = response.data["common_replies"];
    this.multiple_macros = response.data["multiple_macros"];
    this.generated_at = response.data["generated_at"] + "+00:00";
    return this.fetching = false;
  };
  let on_error = response => {
    if (response.status !== 404) {
      FlashService.flash("Sorry, that didn’t go well. Please wait a few seconds and try again.", "error");
    }
    return this.fetching = false;
  };
  MacroSuggestionService.index(this.client.id).then(on_success, on_error);

  this.humanizeDateString = date_string => moment(date_string).fromNow();

  this.distanceClass = distance => {
    if (distance < 0.2) {
      return "low-distance";
    } else if (distance < 0.6) {
      return "mid-distance";
    } else {
      return "high-distance";
    }
  };

  this.distanceScale = distance => {
    if (distance < 0.2) {
      return 1;
    } else if (distance < 0.4) {
      return 2;
    } else if (distance < 0.6) {
      return 3;
    } else if (distance < 0.8) {
      return 4;
    } else {
      return 5;
    }
  };

  this.clusterTicket0Link = macro_suggestion => {
    if (this.client.data_source.name === 'zendesk') {
      return `https://${this.client.name}.zendesk.com/agent/tickets/${macro_suggestion.cluster_ticket_foreign_ids[0]}`;
    } else {
      return `${this.client.url}/${macro_suggestion.cluster_ticket_foreign_ids[0]}`;
    }
  };

  this.macroEditLink = macro_foreign_id => {
    if (this.client.data_source.name === 'zendesk') {
      return `https://${this.client.name}.zendesk.com/agent/admin/macros/edit/${macro_foreign_id}`;
    } else {
      return `${this.client.url}/${macro_foreign_id}`;
    }
  };

  this.sort = column => {
    const is_new_sort_column = (this.sort_column !== column);
    if (is_new_sort_column) {
      if (column === "nearest_macro_distance") {
        this.sort_desc = false;
      } else {
        this.sort_desc = true;
      }
    } else {
      this.sort_desc = !this.sort_desc;
    }
    return this.sort_column = column;
  };

  this.filterAndSortMultipleMacroSuggestions = () => {
    return this.filterAndSort(this.multiple_macros);
  };

  this.filterAndSortCommonReplyMacroSuggestions = () => {
    return this.filterAndSort(this.common_replies);
  };

  this.filterAndSort = list => {
    // filter
    for (var state in this.filters) {
      const is_checked = this.filters[state];
      if (is_checked) {
        list = _.filter(list, ms => ms.state !== state);
      }
    }

    // sort
    const {
      sort_column
    } = this;
    list = _.sortBy(list, ms => ms[sort_column]);
    if (this.sort_desc) { list.reverse(); }
    return list;
  };

  this.create = macro_suggestion => {
    let params;
    if (this.client.data_source.name === 'zendesk') {
      const title = $(`#macro-suggestion-modals-${macro_suggestion.id} .create-title`).val();
      const text = $(`#macro-suggestion-modals-${macro_suggestion.id} .create-text`).val();
      if (!title.length || !text.length) {
        macro_suggestion.modal_error = "Please provide both a title and a comment text.";
        return;
      }

      macro_suggestion.modal_error = null;
      macro_suggestion.show_spinner = true;
      params = {
        "state": "created",
        "create_json": {
          "macro": {
            "active": false,
            "title": title,
            "actions": [
              {
                "field": "comment_value",
                "value": text
              }
            ]
          }
        }
      };
      on_success = response => {
        const macro_foreign_id = response.data.macro.id;
        this.slideUp(macro_suggestion, () => {
          macro_suggestion.show_spinner = false;
          return macro_suggestion.state = 'created';
        });
        this.closeModals();
        return FlashService.flash(`Successfully created Macro \"${title}\". <a href='${this.macroEditLink(macro_foreign_id)}' target='_blank'>View on Zendesk</a>`, "success");
      };
      on_error = response => {
        macro_suggestion.show_spinner = false;
        return macro_suggestion.modal_error = "Sorry, that didn't go well. Please wait a few seconds and try again.";
      };
    } else {
      macro_suggestion.modal_error = null;
      macro_suggestion.show_spinner = true;
      params = {
        "state": "created",
        "create_json": {}
      };
      on_success = response => {
        this.slideUp(macro_suggestion, () => {
          macro_suggestion.show_spinner = false;
          return macro_suggestion.state = 'created';
        });
        return this.closeModals();
      };
      on_error = response => {
        macro_suggestion.show_spinner = false;
        return macro_suggestion.modal_error = "Sorry, that didn't go well. Please wait a few seconds and try again.";
      };
    }

    return MacroSuggestionService.update(this.client.id, macro_suggestion.id, params).then(on_success, on_error);
  };

  this.setState = (macro_suggestion, state) => {
    macro_suggestion.show_spinner = true;
    on_success = response => {
      if (this.filters[state]) {
        return this.slideUp(macro_suggestion, () => {
          macro_suggestion.show_spinner = false;
          return macro_suggestion.state = state;
        });
      } else {
        macro_suggestion.show_spinner = false;
        return macro_suggestion.state = state;
      }
    };
    on_error = response => {
      FlashService.flash("Sorry, that didn’t go well. Please wait a few seconds and try again.", "error");
      return macro_suggestion.show_spinner = false;
    };
    return MacroSuggestionService.update(this.client.id, macro_suggestion.id, {"state": state}).then(on_success, on_error);
  };

  this.slideUp = (macro_suggestion, after) => {
    // execute "after" in case slideUp animation gets interrupted
    let already_called = false;
    const timeoutAfter = function() { if (!already_called) { return after(); } };
    setTimeout(timeoutAfter, 5000);

    const tr = $(`#macro-suggestion-${macro_suggestion.id}`);
    return tr.children('td')
      .animate({ padding: 0 })
      .wrapInner('<div />')
      .children()
      .slideUp(() => {
        tr.children('td>div').unwrap();
        if (tr.is(":visible")) {
          tr.remove();
        }
        after();
        return already_called = true;
    });
  };

  this.substituteText = (macro_suggestion, limitTo) => {
    let text = macro_suggestion.text.trim();
    if (limitTo && (text.length > limitTo)) {
      text = text.slice(0, +limitTo + 1 || undefined);
    }
    for (let key in macro_suggestion.text_substitutions) {
      const value = macro_suggestion.text_substitutions[key];
      text = text.replace(new RegExp(`--#${key}#--`, "g"), `<span class='placeholder'>${value}</span>`);
    }
    return $sce.trustAsHtml(text);
  };

  this.showModal = (macro_suggestion, modal_selector) => {
    const modals = document.getElementById(`macro-suggestion-modals-${macro_suggestion.id}`);
    const modal = modals.querySelector(modal_selector);
    const content_container = modal.querySelector('.content-container');
    const content = modal.querySelector('.content');
    const background = modal.querySelector('.background');

    modal.style.display = 'block';
    content_container.style.top = window.scrollY + 100 + "px";

    // close when clicked OUTSIDE of modal
    background.addEventListener('click', this.closeModals, false);
    content_container.addEventListener('click', this.closeModals, false);

    // don't close when clicked INSIDE of modal
    const stopPropagation = e => e.stopPropagation();
    return content.addEventListener('click', stopPropagation, false);
  };

  this.closeModals = () => {
    const modals = document.querySelectorAll(".macro-suggestion-modal");
    return Array.from(modals).map((modal) =>
      (modal.style.display = 'none'));
  };

  this.hasMacroSuggestions = () => {
    return this.common_replies.length || this.multiple_macros.length;
  };

  this.allMacroSuggestions = () => {
    const list1 = _.map(this.multiple_macros, function(ms) {
      ms.type = 'multiple_macros';
      return ms;
    });
    const list2 = _.map(this.common_replies, function(ms) {
      ms.type = 'common_replies';
      return ms;
    });
    return list1.concat(list2);
  };

  this.createNewEmailTemplateLink = () => {
    return `${this.client.url}/p/email/template/NewEmailTemplateStageManager?setupid=CommunicationTemplatesEmail`;
  };

  this.createNewMacroLink = () => {
    return `${this.client.url}/0JZ/e`;
  };

  this.viewLinkLabel = () => {
    if (this.client.data_source.name === 'zendesk') {
      return "View on Zendesk";
    } else {
      return "View on Salesforce";
    }
  };

  this.ticketsColumnLabel = () => {
    if (this.client.data_source.name === 'zendesk') {
      return "Tickets";
    } else {
      return "Cases";
    }
  };

  this.commonRepliesCSV = () => {
    return _.map(this.filterAndSortCommonReplyMacroSuggestions(), m => {
      return [
        m.text,
        m.num_tickets,
        m.num_agents,
        m.earliest_reply_date,
        m.latest_reply_date,
        this.distanceScale(m.nearest_macro_distance)
      ];
  });
  };

  this.multipleMacrosCSV = () => {
    return _.map(this.filterAndSortMultipleMacroSuggestions(), m => {
      return [
        m.text,
        m.num_tickets,
        m.macro0_title,
        m.macro1_title,
        m.macro0_text,
        m.macro1_text
      ];
  });
  };

  // close modal with escape key
  $document.on("keyup", event => {
    if (event.keyCode === 27) {
      return this.closeModals();
    }
  });

  return this;
});
