/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * DS103: Rewrite code to no longer use __guard__
 * DS207: Consider shorter variations of null checks
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.signup')
.controller('NewUserCtrl', function($state, SignupService, FlashService, me) {
  this.email = me.email;
  this.phone = me.phone;
  this.givenName = me.givenName;
  this.surname = me.surname;
  this.data_source_display_name = me.data_source_display_name;

  this.formSubmit = () => {
    this.loading = true;
    const success = () => $state.go("routing");

    const failure = response => {
      this.loading = false;
      const errorMessage = __guard__(response != null ? response.data : undefined, x => x.message) || "Signup failed";
      return FlashService.flash(errorMessage, "error");
    };

    return SignupService.newUser(this.givenName, this.surname, this.email, this.password, this.phone)
    .then(success, failure);
  };

  return this;
});

function __guard__(value, transform) {
  return (typeof value !== 'undefined' && value !== null) ? transform(value) : undefined;
}