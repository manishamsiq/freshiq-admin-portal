/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.client', [
  'ui.router',
  'wiseSupport.config'
]);

angular.module('wiseSupport.client').config($stateProvider => $stateProvider
  .state('client', {
    url: '/client',
    params: {
      clientId: {
        value: undefined,
        squash: true
      }
    },
    template: '<ui-view />',
    resolve: {
      user(UserService) {
        return UserService.me();
      },

      clientId($stateParams, user) {
        return $stateParams.clientId || user.client_id;
      },

      client(clientId, ClientService) {
        return ClientService.get(clientId);
      },

      route(user, client, ForcedRouteService) {
        return ForcedRouteService.requiresOnboarded(user, client);
      }
    }
  }
));
