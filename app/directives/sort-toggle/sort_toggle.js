/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.sortToggle', [])
  .directive('sortToggle', ($state, $stateParams) => ({
  restrict: 'E',

  scope: {
    name: '@'
  },

  templateUrl: '/directives/sort-toggle/sort_toggle.html',

  link(scope) {
    if ($stateParams.sortBy === scope.name) {
      scope.direction = $stateParams.sortDirection;
    }

    const hasChanged = () => !(($stateParams.sortBy === scope.name) &&
      (scope.direction === $stateParams.sortDirection));

    return scope.$watch("direction", function(direction) {

      if (direction && hasChanged()) {
        return $state.go(".", {
          sortBy: scope.name,
          sortDirection: scope.direction,
          page: 0
        }
        );
      }
    });
  }
}));
