# AnswerIQ.com Confidential and Proprietary

FROM node:latest

RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

COPY package.json /usr/src/app/
RUN npm cache clean
RUN npm install -g gulp bower
RUN npm install
COPY bower.json /usr/src/app/
RUN bower install --allow-root
COPY . /usr/src/app

CMD ["gulp"]
