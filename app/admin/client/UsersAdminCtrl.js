/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.admin')
.controller('UsersAdminCtrl', function(client, UserService, FlashService) {
  this.client = client;
  this.users = null;
  this.loading = true;

  this.makeAdmin = function(user) {
    return UserService.update(this.client.id, user,
      {roles: user.roles.concat("admin")})

    .then(response => _.extend(user, response.data));
  };

  this.removeAdmin = function(user) {
    return UserService.update(this.client.id, user,
      {roles: _.without(user.roles, "admin")})

    .then(response => _.extend(user, response.data));
  };

  this.resetPassword = function(user) {
    if (window.confirm(`Send password reset email to ${user.fullName}?`)) {
      return UserService.sendPasswordResetEmail(user.email).then(() => FlashService
        .flash(`Password reset email sent to ${user.fullName}`, "success"));
    }
  };

  this.deleteUser = function(user) {
    if (window.confirm(`Delete ${user.fullName}?`)) {
      return UserService.delete(this.client.id, user).then(() => {
        return this.users = _.without(this.users, user);
      });
    }
  };

  this.setup = () => {
    this.loading = true;
    return UserService.usersForClient(this.client.id).then(users => {
      this.users = users;
      this.users = _.sortBy(this.users, u => u.fullName.toLowerCase());
      return this.loading = false;
    });
  };

  this.setup();

  return this;
});
