/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.dropdown', [])
  .directive('dropdown', ($document, $timeout) => ({
  restrict: 'AE',

  scope: {
    selectionName: '@'
  },

  link(scope, element) {
    let closeOnDocumentClick;
    element.addClass("dropdown");
    let open = false;

    element.on("click", function() {
      open = !open;
      if (open) {
        element.addClass("open");
        return $timeout(() => $document.on("click", closeOnDocumentClick));
      } else {
        return element.removeClass("open");
      }
    });

    return closeOnDocumentClick = function() {
      $document.off("click", closeOnDocumentClick);
      return element.removeClass("open");
    };
  }
}));
