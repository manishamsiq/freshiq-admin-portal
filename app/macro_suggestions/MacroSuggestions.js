/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.macroSuggestions', [
  'ui.router'
]);

angular.module('wiseSupport.macroSuggestions').config($stateProvider => $stateProvider
  .state('macro-suggestions', {
    parent: 'client',
    url: "/macro-suggestions",
    templateUrl: '/macro_suggestions/index.html',
    controller: 'MacroSuggestionsCtrl as controller'
  }
));
