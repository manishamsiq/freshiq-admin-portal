/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.login', [
  'ui.router',
  'wiseSupport',
  'wiseSupport.config',
  'wiseSupport.flash',
  'wiseSupport.user'
]);

angular.module('wiseSupport.login').config($stateProvider => $stateProvider
  .state('login', {
    url: '/login?destination&destinationParams',
    templateUrl: '/login/login.html'
  }).state('logout', {
    url: '/logout',
    templateUrl: '/login/logout.html'
  }).state('forgotten-password', {
    url: '/forgotten-password',
    templateUrl: '/login/forgotten_password.html'
  }).state('reset-password', {
    url: '/reset-password/:token',
    templateUrl: '/login/reset_password.html'
  }
));
