/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

describe("TicketFieldService:", function() {
  beforeEach(module("wiseSupport.ticketFields"));

  beforeEach(inject(function(TicketFieldService,
                     $httpBackend,
                     config,
                     $rootScope,
                     $q) {
    this.TicketFieldService = TicketFieldService;
    this.$httpBackend = $httpBackend;
    this.config = config;
    this.$q = $q;
    this.scope = $rootScope.$new();

    this.lastFetchedAt = 123;

    this.ticketFieldsResponse = {
      last_fetched_at: this.lastFetchedAt,
      ticket_fields: [{
        Assignee: { id: 5 },
        Priority: { id: 6 }
      }
      ]
    };

    this.clientId = 42;
    return this.url = `${this.config.apiBase}/api/v1/clients/${this.clientId}/ticket-fields`;
  })
  );

  describe("url()", () => it("computes the URL from the clientId on ClientService", function() {
    const expected = `${this.config.apiBase}/api/v1/clients/${this.clientId}/ticket-fields`;
    return expect(this.TicketFieldService.url(this.clientId)).toBe(expected);
  }));

  describe("fetch()", function() {
    beforeEach(function() {
      this.fetchUrl = this.url + "/fetch";
      this.$httpBackend.whenPOST(this.fetchUrl).respond();
      return this.result = this.TicketFieldService.fetch(this.clientId);
    });

    afterEach(function() { return this.$httpBackend.flush(); });

    return it("POSTs to /ticket-fields to start ticket field fetching", function() {
      return this.$httpBackend.expectPOST(this.fetchUrl);
    });
  });


  describe("ticketFields()", function() {
    beforeEach(function() {
      this.$httpBackend.whenGET(this.url).respond(this.ticketFieldsResponse);
      return this.resultPromise = this.TicketFieldService.ticketFields(this.clientId);
    });

    afterEach(function() {
      return this.$httpBackend.flush();
    });

    it("gets from url", function() {
      return this.$httpBackend.expectGET(this.url);
    });

    return it("populates the list with ticket fields", function() {

      const ticketFields = _.map(this.ticketFieldsResponse.ticket_fields, field => {
        return new this.TicketFieldService.TicketField(field);
      });

      return this.resultPromise.then(result => // Use toString here to handle deep equality.
      expect(result.toString()).toEqual(ticketFields.toString()));
    });
  });


  describe("enabledTicketFields()", function() {
    beforeEach(function() {
      this.url += "?enabled=true";
      this.$httpBackend.whenGET(this.url).respond(this.ticketFieldsResponse);
      return this.resultPromise = this.TicketFieldService.enabledTicketFields(this.clientId);
    });

    afterEach(function() {
      return this.$httpBackend.flush();
    });

    it("gets from url", function() {
      return this.$httpBackend.expectGET(this.url);
    });

    return it("populates the list with ticket fields", function() {

      const ticketFields = _.map(this.ticketFieldsResponse.ticket_fields, field => {
        return new this.TicketFieldService.TicketField(field);
      });

      return this.resultPromise.then(result => // Use toString here to handle deep equality.
      expect(result.toString()).toEqual(ticketFields.toString()));
    });
  });


  return describe("alterFields()", function() {
    beforeEach(function() {
      this.fields = [1, 2, 3];

      this.$httpBackend.whenPUT(this.url).respond();
      return this.resultPromise = this.TicketFieldService.alterFields(this.clientId, this.fields);
    });

    afterEach(function() {
      return this.scope.$digest();
    });

    it("PUTSs its args to /ticket-fields", function() {
      return this.$httpBackend.expectPUT(this.url,
        {ticket_fields: this.fields});
    });

    return it("returns a promise", function() {
      return this.resultPromise.then(result => expect(true));
    });
  });
});
