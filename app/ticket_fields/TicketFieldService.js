/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.ticketFields')
.service('TicketFieldService', function($http, config, $filter) {

  let TicketField;
  return {
    TicketField: (TicketField = class TicketField {
      constructor(json) {
        _.extend(this, json);
        this.original = json;
        this.wasEnabled = this.enabled;
        this.optionTagsCSV = _.map(this.options, option => [option.name, option.tag]);
      }

      resetConfiguration() {
        return this.model_configuration_string = $filter('json')(this.model_configuration);
      }

      unsavedConfiguration() {
        return this.model_configuration_string !== $filter('json')(this.model_configuration);
      }

      addOption() {
        return this.options.push({
          name: "",
          value: ""
        });
      }

      removeOption(index) {
        return this.options.splice(index, 1);
      }

      getOption(id) {
        return _.find(this.options, option => option.id === parseInt(id));
      }
    }),


    url(clientId) {
      return config.apiBase + `/api/v1/clients/${clientId}/ticket-fields`;
    },

    fetch(clientId) {
      return $http.post(this.url(clientId) + "/fetch");
    },

    ticketFields(clientId) {
      return $http.get(this.url(clientId)).then(response => {
        let fields = response.data.ticket_fields;
        fields = _.map(fields, field => new this.TicketField(field));
        fields = _.sortBy(fields, 'name');
        fields = _.sortBy(fields, field => !field.enabled);
        response.data.ticket_fields = fields;
        return response.data;
      });
    },

    enabledTicketFields(clientId) {
      return $http.get(this.url(clientId), { params: {
        enabled: true
      }
    }
      ).then(response => {
        let fields = response.data.ticket_fields;
        fields = _.map(fields, field => new this.TicketField(field));
        fields = _.sortBy(fields, 'name');
        return fields;
      });
    },

    ticketField(clientId, fieldId) {
      return $http.get(this.url(clientId) + "/" + fieldId).then(response => {
        return new this.TicketField(response.data);
      });
    },

    wiseConfidentField(clientId) {
      const on_success = response => {
        return new this.TicketField(response.data);
      };
      const on_error = response => {
        return {};
      };
      return $http.get(`${this.url(clientId)}/wise-confident-field`).then(on_success, on_error);
    },

    alterFields(clientId, fields) {
      return $http.put(this.url(clientId), {ticket_fields: fields});
    },

    alterField(clientId, fieldId, data) {
      return $http.patch(this.url(clientId) + `/${fieldId}`,
        {ticket_field: data},
        {ignoreLoadingBar: true})
      .then(response => {
        return new this.TicketField(response.data);
      });
    },

    rebuildField(clientId, fieldId) {
      return $http.post(this.url(clientId) + `/${fieldId}/rebuild`);
    },

    deleteValues(clientId, fieldId) {
      return $http.post(this.url(clientId) + `/${fieldId}/delete-values`);
    },

    rebuildFields(client) {
      if (window.confirm(`Rebuild all fields for ${client.name}?`)) {
        return $http.post(this.url(client.id) + "/rebuild");
      }
    },

    newField(clientId, field) {
      return $http.post(this.url(clientId), {
        ticket_field: {
          name: field.name,
          options: field.options,
          zendesk_id: field.zendesk_id,
          type: field.type
        }
      }).then(response => {
        return new this.TicketField(response.data);
      });
    },

    writeField(clientId, field) {
      return $http.post(this.url(clientId)  + "/" + field.id).then(response => {
        return new this.TicketField(response.data);
      });
    },

    deleteField(clientId, field) {
      return $http.delete(this.url(clientId) + "/" + field.id);
    },

    fetchModelConfig(clientId, field) {
      return $http.get(`${config.apiBase}/api/v1/clients/${clientId}/ticket-fields/${field.id}/config`).then(function(response) {
        const model_configuration = response.data;
        field.model_configuration = model_configuration;
        return field.model_configuration_string = $filter('json')(model_configuration);
      });
    }
  };
});
