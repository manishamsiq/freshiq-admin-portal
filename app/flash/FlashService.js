/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.flash')
.service('FlashService', function($sce) {

  return {
    message: "",
    visible: false,
    type: "notice",

    flash(message, type) {
      this.type = type;
      this.message = $sce.trustAsHtml(message);
      return this.visible = true;
    },

    close() {
      return this.visible = false;
    }
  };
});
