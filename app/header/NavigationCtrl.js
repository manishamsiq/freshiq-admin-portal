/*
 * decaffeinate suggestions:
 * DS101: Remove unnecessary use of Array.from
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport')
.controller('NavigationCtrl', function($state, UserService) {
  this.admin = false;
  UserService.me().then(user => {
    return this.admin = (user && Array.from(user.roles).includes("admin"));
  });

  const activeState = $state.current.name.split(".");

  this.isActive = state => state === activeState[0];

  return this;
});
