/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.reports', [
  'ui.router'
]);

angular.module('wiseSupport.reports').config($stateProvider => $stateProvider
  .state('reports', {
    parent: 'client',
    url: '/reports',
    templateUrl: '/reports/show.html',
    controller: 'ReportsCtrl as controller'
  }
));
