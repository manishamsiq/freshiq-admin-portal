/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.signup')
.controller('FinalizeCtrl',
function($state, client, ClientService) {

  // mark the client as onboarded in the backend
  ClientService.patch(client.id, {onboarded: true});

  this.formSubmit = function() {
    this.loading = true;
    return location.href = '/client/routing';
  };

  return this;
});
