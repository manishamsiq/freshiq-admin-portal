/*
 * decaffeinate suggestions:
 * DS101: Remove unnecessary use of Array.from
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.response')
.controller('ResponseCtrl',
function(client, MacroModelService, UserService, FlashService, MacroService) {
  this.client = client;
  this.loading = true;
  this.models = null;
  this.open = null;
  this.saved = false;
  this.preview = {
    response: null,
    macro_foreign_id: null,
    ticket_foreign_id: null,
    template: this.client.dmc_substitutions_template
  };
  this.admin = false;
  UserService.me().then(user => {
    return this.admin = (Array.from(user.roles).includes("admin"));
  });

  this.admin = false;
  UserService.me().then(user => {
    return this.admin = (Array.from(user.roles).includes("admin"));
  });

  this.previewResponse= function() {
    const on_success = response => {
      this.preview.response = response.data.preview.comment.body;
      return this.preview.api_results = response.data.api_results;
    };
    const on_error = response => {
      this.preview.response = null;
      this.preview.api_results = null;
      let message = response.data.error;
      if (this.admin && response.data.details && response.data.details.exception) {
         message = message + ' Details: ' + response.data.details.exception;
       }
      return FlashService.flash(message, "error");
    };

    if (this.preview.ticket_foreign_id === null) {
      return FlashService.flash("Please enter Ticket ID.", "error");
    } else {
      FlashService.close();
      return MacroService.previewMacro(this.client.id, {
        ticket_foreign_id: this.preview.ticket_foreign_id,
        macro_foreign_id: this.preview.macro_foreign_id,
        template: this.preview.template
      }).then(on_success, on_error);
    }
  };

  this.toggleAutoResponseEnabled = function(model) {
    return MacroModelService.alterModel(this.client, model.id,
      {auto_response_enabled: model.auto_response_enabled})
    .then(newModel => {
      this.saved = true;
      return model.constructor(newModel.original);
    });
  };

  this.setup = () => {
    return MacroModelService.macroModels(this.client).then(models => {
      this.models = models;
      this.open = this.models[0];
      this.loading = false;
      return Array.from(models).map((model) =>
        MacroModelService.fetchModelConfig(this.client, model));
    });
  };

  this.setup();

  return this;
});
