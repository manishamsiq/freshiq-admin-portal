/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.flash')
.controller('FlashCtrl', function(FlashService, $stateParams, $rootScope) {

  if ($stateParams.errorMessage) {
    FlashService.flash($stateParams.errorMessage, "error");
  }

  this.close = () => FlashService.close();

  this.visible = () => FlashService.visible;

  this.message = () => FlashService.message;

  this.type = () => 'flash-' + FlashService.type;

  $rootScope.$on('$stateChangeSuccess', () => FlashService.close());

  return this;
});
