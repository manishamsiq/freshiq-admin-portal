/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.admin')
.service('FromAddressesService', function($http, config) {

  let FromAddress;
  return {
    FromAddress: (FromAddress = class FromAddress {
      constructor(json) {
        _.extend(this, json);
        this.name = this.foreign_attributes.name;
        this.display_name = this.name + " (ID: " + this.foreign_id + ")";
      }
    }),

    fetchFromAddresses(clientId) {
      const url = config.apiBase + `/api/v1/clients/${clientId}/org-wide-email-addresses`;
      return $http.get(url).then(response => {
        return _.map(response.data, fromAddress => {
          return new this.FromAddress(fromAddress);
        });
      });
    }
  };
});
