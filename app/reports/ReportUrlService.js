/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.reports')
.service('ReportUrlService', ($http, $httpParamSerializer, config) => ({
  get(client_id, predictable_id) {
    const url = `${config.apiBase}/api/v1/clients/${client_id}/report_urls/${predictable_id}`;
    return $http.get(url).then(function(api_response) {
      const periscope_url = api_response.data;
      return periscope_url;
    });
  }
}));
