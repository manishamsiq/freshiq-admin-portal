/*
 * decaffeinate suggestions:
 * DS102: Remove unnecessary code created because of implicit returns
 * Full docs: https://github.com/decaffeinate/decaffeinate/blob/master/docs/suggestions.md
 */
// AnswerIQ.com Confidential and Proprietary

angular.module('wiseSupport.admin')
.service('UnifiedWidgetConfigService', ($http, config) => ({
  put(client_id, agent_foreign_id, json_config, description) {
    return $http({
      method: 'PUT',
      url: `${config.apiBase}/api/v1/clients/unified_widget_configs`,
      data: {
        "client_id": client_id,
        "agent_foreign_id": agent_foreign_id,
        "config": json_config,
        "description": description},
      headers: {
        "Content-Type": 'x-www-form-urlencoded'}});
  },

  delete(client_id, agent_foreign_id, description) {
    let url = `${config.apiBase}/api/v1/clients/unified_widget_configs?client_id=${client_id}&description=${description}`;
    if (agent_foreign_id) {
      url = url + `&agent_foreign_id=${agent_foreign_id}`;
    }
    return $http({
      method: 'DELETE',
      url});
  },

  all(client_id) {
    return $http.get(`${config.apiBase}/api/v1/clients/unified_widget_configs?client_id=${client_id}`).then(r => r.data);
  }
}));
